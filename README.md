cabinwebproject
===============

cabinwebproject is a web application where users can find cabins based on a preferred weather condition. The application operates on semantic data. The data sets are not available in semantic formats and the application have to retrive the data sets from yr.no and Den Norske Turistforening (https://www.turistforeningen.no/) and lift them semantically before they can be used. 

Uses Jena and a local TDB. 

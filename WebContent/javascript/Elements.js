

/* Elements constructor
 * Holds a reference to all relevant DOM elements
 */
var Elements = function() {
	this.weatherInputDataAttr = 'data-weather-type-input';
	this.weatherInputLabelDataAttr = 'data-weather-type';
	
	this.weatherInputElems = this.getElementsByAttribute('input', this.weatherInputDataAttr);
	this.weatherInputLabelElems = this.getElementsByAttribute('label', this.weatherInputLabelDataAttr);
	
	this.activeWeatherElemWrapper = document.getElementById('active_weather_type_wrapper');
	this.activeWeatherElem = this.retrieveActiveWeatherElem();
	
	this.activeCountyLabelElem = null;
	this.countyLabelElems = this.buildCountyTabLabelElems();
	
	this.municipalityPreviewWrapperElem = document.getElementById('municipality_preview_wrapper');
	this.municipalityInputElems = this.buildMunicipalityInputElems();
	
	this.toggleSearchCriteriaInput = document.getElementById('search_criteria_section_toggle_input');
	this.toggleSearchCriteriaLabel = document.getElementById('search_criteria_section_toggle_label');
};



Elements.prototype.buildCountyTabLabelElems = function() {
	return this.getElementsByAttribute('label', 'data-county-tab-label');
};



Elements.prototype.getElementsByAttribute = function(type, attr) {
	var allElems = document.getElementsByTagName(type);
	var attrElems = [];
	
	var elemNum = allElems.length;
	for(var i = 0; i < elemNum; i++) {
		var elem = allElems[i];

		if(elem.getAttribute(attr) != null) {
			attrElems.push(elem);
		}
	}
	
	return attrElems;
};


Elements.prototype.retrieveActiveWeatherElem = function() {
	var inputs = this.weatherInputElems;
	var labels = this.weatherInputLabelElems;
	
	var checkedInputElem = null;
	inputs.forEach(function(inputElem) {
		if(inputElem.getAttribute('checked') == 'checked') {
			checkedInputElem = inputElem;
		}
	});
	
	var activeWeatherElem = null;
	labels.forEach(function(labelElem) {
		var labelAttr = labelElem.getAttribute(this.weatherInputLabelDataAttr);
		var inputAttr = checkedInputElem.getAttribute(this.weatherInputDataAttr);
		
		if(labelAttr == inputAttr) {
			activeWeatherElem = this.createActiveWeatherElem(labelElem);
		}
	}.bind(this));
	
	return activeWeatherElem;
};



Elements.prototype.createActiveWeatherElem = function(wrapperElem) {
	var activeWeatherElem = wrapperElem.getElementsByTagName('div')[0].cloneNode(true);
	activeWeatherElem.className += ' active_weather_type_wrapper';
	
	return activeWeatherElem;
};


Elements.prototype.renderActiveWeatherElem = function() {
	this.activeWeatherElemWrapper.textContent = '';
	this.activeWeatherElemWrapper.appendChild(this.activeWeatherElem);
};



Elements.prototype.getNearestAncestor = function(type, elem) {
	return (elem.tagName.toLowerCase() == type.toLowerCase()) ? elem: Elements.prototype.getNearestAncestor(type, elem.parentNode);
};


Elements.prototype.buildMunicipalityInputElems = function() {
	var inputElems = this.getElementsByAttribute('input', 'data-municipality-input');
	
	return inputElems;
};



Elements.prototype.buildMunicipalityPreviewElem = function(parentElem) {
	var previewElemWrapper = parentElem.cloneNode(true);
	var inputElem = previewElemWrapper.getElementsByTagName('input')[0];
	var labelElem = previewElemWrapper.getElementsByTagName('label')[0];
	
	var previewId = 'preview_' + inputElem.getAttribute('id');
	
	inputElem.className += ' hidden_element';
	labelElem.className += ' tag_padding rounded_edges';
	
	inputElem.setAttribute('id', previewId);
	labelElem.setAttribute('for', previewId);

	inputElem.name = 'preview_' + inputElem.name;
	
	return previewElemWrapper;
};


Elements.prototype.retrieveMunicipalityPreviewElems = function() {
	var elems = this.municipalityPreviewWrapperElem.getElementsByTagName("div");
	return elems;
};


Elements.prototype.retrieveMunicipalityInputElems = function(muniCodes, foundCallback) {
	this.municipalityInputElems.forEach(function(inputElem) {
		var inputCode = inputElem.getAttribute('data-municipality-input');
		
		muniCodes.forEach(function(muniCode) {
			if(inputCode == muniCode) foundCallback(inputElem);
		});
	});
};


Elements.prototype.updateActiveWeatherElem = function(tokenType) {
	this.weatherInputElems.forEach(function(inputElem) {
		var type = inputElem.getAttribute(this.weatherInputDataAttr);
		
		if(type == tokenType) {
			var labelElem = inputElem.nextElementSibling;
			var activeElem = this.createActiveWeatherElem(labelElem);
			this.activeWeatherElem = activeElem;

			var prevActive = this.weatherInputElems[0];
			prevActive.removeAttribute('checked');
			inputElem.checked = true;
		}
	}.bind(this));
};







var Loader = {
	load: function() {
		new Main();
	}
};
window.addEventListener('DOMContentLoaded', Loader.load, false);
//window.addEventListener('load', Loader.load, false);





/* Main constructor
 * Runs the show
 */
var Main = function() {
	this.elements = new Elements();
	
	this.addListeners();
	this.setDefaultState();
};


Main.prototype.addListeners = function() {
	this.addCountyTabListener();
	this.addSelectedWeatherTypeListener();
	this.addMunicipalityListeners();
	this.addToggleSearchCriteriaListener();
};


Main.prototype.addCountyTabListener = function() {
	if(this.elements.countyLabelElems == null) { return; }
	
	this.elements.countyLabelElems.forEach(function(countyLabelElem) {
		countyLabelElem.addEventListener('click', this.toggleActiveCountyLabel.bind(this), false);
	}.bind(this));
	
};


Main.prototype.addSelectedWeatherTypeListener = function() {
	var labelElems = this.elements.weatherInputLabelElems;
	if(labelElems == null) { return; }
	
	labelElems.forEach(function(labelElem) {
		labelElem.addEventListener('click', this.selectActiveWeatherType.bind(this), false);
	}.bind(this));
};


Main.prototype.addMunicipalityListeners = function() {
	this.elements.municipalityInputElems.forEach(function(inputElem) {
		inputElem.addEventListener('click', this.togglePreviewMunicipality.bind(this), false);
	}.bind(this));
};


Main.prototype.addToggleSearchCriteriaListener = function() {
	this.elements.toggleSearchCriteriaInput.addEventListener('click', this.updateToggleSearchCriteriaState.bind(this), false);
};


Main.prototype.setDefaultState = function() {
	this.setDefaultWeatherType();
	this.setDefaultCountyLabel();
	this.setActiveMunicipalityInputElems();
	this.updateToggleSearchCriteriaState();
};


Main.prototype.setDefaultWeatherType = function() {
	var activeWeatherElemWrapper = this.elements.activeWeatherElemWrapper;
	var searchedWeatherTypes = this.retrieveGetParam('weather_type');
	
	if(searchedWeatherTypes) {
		var searchedWeatherType = searchedWeatherTypes[0];
		this.elements.updateActiveWeatherElem(searchedWeatherType);
	}
	
	var activeWeatherElem = this.elements.activeWeatherElem;
	this.elements.renderActiveWeatherElem();
//	activeWeatherElemWrapper.appendChild(activeWeatherElem);
};


Main.prototype.setDefaultCountyLabel = function() {
	this.elements.countyLabelElems.forEach(function(labelElem) {
		var labelValue = labelElem.innerHTML.trim().toLowerCase();
		if(labelValue == 'hordaland') {
			/*labelElem.setAttribute('checked', 'checked');
			labelElem.className += " active";

			this.elements.activeCountyLabelElem = labelElem;*/
			
			this.fireEvent(labelElem, 'click');
		}
	}.bind(this));
};


Main.prototype.selectActiveWeatherType = function(e) {
	var selectedWeatherElemWrapper = Elements.prototype.getNearestAncestor('label', e.target);
	this.elements.activeWeatherElem = this.elements.createActiveWeatherElem(selectedWeatherElemWrapper);
	this.elements.renderActiveWeatherElem();
};


Main.prototype.toggleActiveCountyLabel = function(event) {
	var elem = event.target;
	
	if(this.elements.activeCountyLabelElem != null) {
		this.elements.countyLabelElems.forEach(function(listedElem) {
			if(listedElem == this.elements.activeCountyLabelElem) {
				var inactiveClassName = this.elements.activeCountyLabelElem.className.replace(' active', '');
				this.elements.activeCountyLabelElem.className = inactiveClassName;
			}
		}.bind(this));
	}
	
	elem.className += " active";
	this.elements.activeCountyLabelElem = elem;
};


Main.prototype.fireEvent = function(elem, event) {
	if (document.createEvent) {
		// dispatch for firefox + others
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent(event, true, true); // event type,bubbling,cancelable
		return !elem.dispatchEvent(evt);
	}
	else {
		// dispatch for IE
		var evt = document.createEventObject();
		return elem.fireEvent('on' + event, evt);
	}
};



Main.prototype.togglePreviewMunicipality = function(e) {
	var inputElem = e.target;
	var muniCode = inputElem.getAttribute('data-municipality-input');
	
	if(inputElem.checked) { // it changes checked on listener before this
		this.pinMunicipalityPreview(inputElem.parentNode);
	}
	else {
		this.unpinMunicipalityPreview(muniCode);
	}
	//var inputLabel = inputElem.nextElementSibling;

};


Main.prototype.pinMunicipalityPreview = function(originalWrapper) {
	var previewElem = this.elements.buildMunicipalityPreviewElem(originalWrapper);
	var inputElem = previewElem.getElementsByTagName('input')[0];
	
	inputElem.addEventListener('click', this.unpinPreviewMunicipality.bind(this), false);
	
	this.elements.municipalityPreviewWrapperElem.appendChild(previewElem);
};


Main.prototype.unpinMunicipalityPreview = function(muniCode) {
	var previewElems = this.elements.retrieveMunicipalityPreviewElems();
	var foundElem = null;
	
	for(var i = 0; i < previewElems.length; i++) {
		var previewWrapperElem = previewElems[i];
		var foundMuniCode = previewWrapperElem.getElementsByTagName('input')[0].getAttribute('data-municipality-input');
		
		if(foundMuniCode == muniCode) {
			foundElem = previewWrapperElem;
		}
	}
	
	if(foundElem)
		this.elements.municipalityPreviewWrapperElem.removeChild(foundElem);
};


Main.prototype.unpinPreviewMunicipality = function(e) {
	var previewInputElem = e.target;
	var previewWrapperElem = e.target.parenNode;
	
	var foundInput = null;
	
	var muniCode = previewInputElem.getAttribute('data-municipality-input');
	
	var inputElems = this.elements.municipalityInputElems;
	for(var i = 0; i < inputElems.length; i++) {
		var inputElem = inputElems[i];
		
		var foundMuniCode = inputElem.getAttribute('data-municipality-input');
		if(muniCode == foundMuniCode) {
			inputElem.checked = false;
			foundInput = inputElem;
		}
	}
	
//	this.unpinMunicipalityPreview(muniCode);
	
	if(foundInput) {
		this.unpinMunicipalityPreview(muniCode);
//		this.elements.municipalityPreviewWrapperElem.removeChild(foundInput.parentNode);
//		this.fireEvent(foundInput, 'click');
	}
};


Main.prototype.setActiveMunicipalityInputElems = function() {
	var activeMuniCodes = this.retrieveGetParam('municipalities');
	
	if(!activeMuniCodes) return;
	
	this.elements.retrieveMunicipalityInputElems(activeMuniCodes, function(inputElem) {
//		this.fireEvent(inputElem, 'clicked');
		inputElem.checked = true;
		this.pinMunicipalityPreview(inputElem.parentNode);
	}.bind(this));
};



Main.prototype.retrieveGetParam = function(paramLabel) {
	var paramStr = window.location.search.slice(1);
	var paramPairs = paramStr.split('&');
	
	var params = {};
	
	paramPairs.forEach(function(paramStr) {
		var param = paramStr.split('=');
		var label = param[0];
		var value = param[1];
		
		if(!params[label]) params[label] = [];
		
		params[label].push(value);
	});
	
	return params[paramLabel] || null;
};



Main.prototype.updateToggleSearchCriteriaState = function() {
	var input = this.elements.toggleSearchCriteriaInput;
	var label = this.elements.toggleSearchCriteriaLabel;
	
	var baseMsg = ' søkevalgene';
	var disabledMsg = 'Viser' + baseMsg;
	var showMsg = 'Vis' + baseMsg;
	var hideMsg = 'Skjul' + baseMsg;
	
	if(input.disabled) {
		label.textContent = disabledMsg;
	}
	else {
		if(!input.disabled && input.checked)
			label.textContent = showMsg;
		
		else if(!input.disabled && !input.checked)
			label.textContent = hideMsg;
	}
		
};





<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="no.uib.info216.cabinwebproject.view.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%
	List<NavItem> primaryNav = (List<NavItem>) request.getAttribute("primaryNav");
%>

<!DOCTYPE html>
<html lang="no">

<head>
	<title>Hyttevær</title>
	
	<meta charset="utf-8">
	<meta name="robots" content="index, nofollow">
	
	<link rel="stylesheet" href="${contextPath}/stylesheet/style.css">
	<script type="text/javascript" src="${contextPath}/javascript/Elements.js"></script>
	<script type="text/javascript" src="${contextPath}/javascript/Main.js"></script>
</head>

<body prefix="schema: http://schema.org/">
	
	<div id="webdocument_wrapper">
		
		<header id="webdocument_header_wrapper" class="section_padding section_margin_bottom">
			<section class="space_between_wrapper section_margin_bottom">
				
				<div class="side_by_side_wrapper section_margin_right">
					<span>&#8962;</span>
					<div class="column_wrapper center_alignment">
						<h1 id="site_title"><a href="/cabinwebproject/index">Hyttevær</a></h1>
						<p id="site_subtitle">Du finner været, vi finner hyttene!</p>
					</div>
				</div>
				
				<aside class="section_margin_left">
					<p>Dette er en semantisk applikasjon som gir deg muligheten til å
						finne ut hvordan været blir på Turistforeningens hytter. 
					</p>
				</aside>
				
			</section>
			
			<nav>
				<% for(NavItem navItem : primaryNav) { %>
					<a href="<%= navItem.getHref() %>"><%= navItem.getLabel() %></a>
				<% } %>
			</nav>
		</header>
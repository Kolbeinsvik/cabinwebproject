<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="no.uib.info216.cabinwebproject.model.*"%>
<%@page import="no.uib.info216.cabinwebproject.view.*"%>
<%
	List<CountyModel> counties = (List<CountyModel>) request.getAttribute("counties"); 
	List<MunicipalityModel> municipalities = (List<MunicipalityModel>) request.getAttribute("municipalities");
	List<SemanticCabinModel> foundCabins = (List<SemanticCabinModel>) request.getAttribute("foundCabins");
	List<WeatherTypeView> weatherTypes = (List<WeatherTypeView>) request.getAttribute("weatherTypes");
	WeatherTypeModel weatherType = (WeatherTypeModel) request.getAttribute("weatherType");
	
	String weatherTypeUnicode = WeatherTypeView.getUnicodeIcon(weatherType.getTokenLabel());
%>


<article class="stand_out_wrapper section_padding section_vertical_margins">
	<header>
		<h1 class="section_heading fancy_underlined heading_bottom_margin">Vi fant <%= foundCabins.size() %> hytter hvor det er 
			<span>
				<span><%= weatherTypeUnicode %></span> 
				<%= weatherType.getLabel() %>
			</span>
		</h1>
	</header>
	
	<section id="found_cabins_wrap" class="space_between_wrapper flex_wrap flex_elements_equal_size">
		<% for(SemanticCabinModel cabin : foundCabins) { %>
			<% if(cabin == null) {
				System.err.println("Searched for cabin. Found cabin is null. ");
				continue;
			} %>
			<% String rainSpan = (cabin.getWeather().getPrecipitation() > 0) ? " <span>" + cabin.getWeather().getPrecipitation() + " mm nedb&oslash;r</span>": ""; %>
			
			<section class="section_margin_left section_margin_right section_vertical_margins found_cabins">
				<div class="side_by_side_wrapper">
					<div><img src="<%= cabin.getImagePath() %>" alt="Bilde av hytten" /></div>
					<div class="column_wrapper">
						<h2><%= cabin.getEscapedLabel() %></h2>
						<aside>
							<p>
								<span><span><%= weatherTypeUnicode %></span> <%= cabin.getWeather().getTemperature() %>&deg;C</span>
								<span><%= cabin.getEscapedMunicipality() %> (<%= cabin.getEscapedCounty() %>)</span>
								<%= rainSpan %> <br>
								<span><a href="<%= cabin.getUtUrl() %>">Les mer om <%= cabin.getEscapedLabel() %> p&aring; ut.no </a></span>
							<p>
						</aside>
					</div>
				</div>
			</section>
		<% } %>
	</section>

</article>
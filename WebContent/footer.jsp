<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>	
		<footer>
			<p typeof="schema:Organization">
				Hyttedata fra <a property="schema:url" href="https://www.turistforeningen.no/" target="_blank">
				<span property="schema:legalName">Den Norske Turistforening</span></a>. 
				Tilgjengelig under <a property="http://creativecommons.org/ns#license" href="http://creativecommons.org/licenses/by-nc/3.0/no/" target="_blank">CC BY-NC 3.0 NO lisens</a>.
			</p>
			<p typeof="schema:Organization">
				<a typeof="schema:Thing" propterty="schema:url" href="http://om.yr.no/verdata/" target="_blank">Gratis v&aelig;rdata</a> fra 
				<a property="schema:url" href="http://www.yr.no" target="_blank">yr.no</a>, levert av
				<span property="schema:legalName">Meteorologisk institutt</span> og <span property="schema:legalName">NRK</span>. 
				Tilgjengelig under <a property="http://creativecommons.org/ns#license" href="http://creativecommons.org/licenses/by-nc/3.0/no/">CC BY-NC 3.0 NO lisens</a>
			</p>
		</footer>
	
	</div><%-- #webdocument_wrapper --%>

</body>
</html>
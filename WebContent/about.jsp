<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="no.uib.info216.cabinwebproject.model.*"%>
<%@page import="no.uib.info216.cabinwebproject.view.*"%>

<jsp:include page="header.jsp" />


<div id="search" class="activity_wrapper">
	
	<article class="stand_out_wrapper section_padding">
		<header>
			<h1 class="fancy_underlined heading_bottom_margin">Om oss</h1>
		</header>
		
		<section class="section_vertical_margins">
			<p>Dette er en semantisk applikasjon som gir deg muligheten til &aring;
				finne ut hvordan v&aelig;ret blir p&aring; Turistforeningens hytter. Du kan velge
				hvilken v&aelig;rtype du &oslash;nsker, og hvilket omr&aring;de du &oslash;nsker &aring; reise til.
				S&oslash;ket vil gi deg en oversikt over hytter som har dette v&aelig;ret og
				ligger i det aktuelle omr&aring;det. Applikasjonen benytter hyttedata fra
				Den Norske Turistforening og v&aelig;rdata fra Yr.
			</p>
		</section>
		
		<section class="section_vertical_margins">
			<h2 class="fancy_underlined heading_bottom_margin">Lisenser</h2>
			
			<ul>
				<li>Hyttedata fra <span property="legalName">Den Norske Turistforening</span>. Tilgjengelig under <a property="http://creativecommons.org/ns#license" href="http://creativecommons.org/licenses/by-nc/3.0/no/">CC BY-NC 3.0 NO lisens</a>.</li> 
				<li>Gratis v&aelig;rdata fra yr.no, levert av <span property="legalName">Meteorologisk institutt</span> og <span property="legalName">NRK</span>Tilgjengelig under <a property="http://creativecommons.org/ns#license" href="http://creativecommons.org/licenses/by-nc/3.0/no/">CC BY-NC 3.0 NO lisens</a></li>
			</ul>
		</section>
	</article>
	
</div>

<jsp:include page="footer.jsp" />

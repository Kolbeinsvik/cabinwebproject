<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="no.uib.info216.cabinwebproject.model.*"%>
<%@page import="no.uib.info216.cabinwebproject.view.*"%>


<% 
	List<CountyModel> counties = (List<CountyModel>) request.getAttribute("counties"); 
	List<MunicipalityModel> municipalities = (List<MunicipalityModel>) request.getAttribute("municipalities");
	List<SemanticCabinModel> foundCabins = (List<SemanticCabinModel>) request.getAttribute("foundCabins");
	List<WeatherTypeView> weatherTypes = (List<WeatherTypeView>) request.getAttribute("weatherTypes");
	WeatherTypeModel activeWeatherType = (WeatherTypeModel) request.getAttribute("weatherType");
	
	boolean hasSearched = (Boolean) request.getAttribute("hasSearched");
	boolean anyCabinsFound = (foundCabins.size() > 0) ? true: false;
	
	String checkedSearchCriteriaToggleStr = (hasSearched && anyCabinsFound) ? "checked=\"checked\"": "";
	String disabledSearchCriteriaToggleStr = (!hasSearched || (hasSearched && !anyCabinsFound)) ? " disabled=\"disabled\"": "";
%>

<jsp:include page="header.jsp" />


<div id="search" class="activity_wrapper">
	
	<form action="" method="get">
		<input id="search_criteria_section_toggle_input" 
		type="checkbox" name="search_criteria_section_toggle" 
		<%= checkedSearchCriteriaToggleStr %> <%= disabledSearchCriteriaToggleStr %> value="" />
		
		<header class="stand_out_wrapper section_padding section_vertical_margins">
			<h1 class="section_heading fancy_underlined heading_bottom_margin">Mine s&oslash;kekriteria:</h1>
			
			<div class="side_by_side_wrapper section_margins">
				<section id="active_weather_type_wrapper" class="section_margin_right" data-active-search="weather_type"></section>
				
				<section id="municipality_preview_wrapper" class="section_margin_left section_margin_right" data-active-search="area">
				</section>
				
				<div class="section_margin_left">
					<aside class="info_box heading_bottom_margin">Om ingen kommuner er valgt vil s&oslash;ket omfatte <em>alle kommuner</em>.</aside>
					
					<div class="space_between_wrapper equal_flex">
						<button class="btn section_margin_right" type="submit" name="action" value="search">S&oslash;k</button>
						<label id="search_criteria_section_toggle_label" class="btn section_margin_left" for="search_criteria_section_toggle_input">Viser s&oslash;kevalgene</label>
					</div>
				</div>
			</div>
			
		</header>
		
		<% if(hasSearched && !anyCabinsFound) { %>
			<section class="section_info_box section_padding section_vertical_margins">
				<span class="section_heading">Søket fant ingen hytter.</span>
			</section>
		<% } %>
		
		<section id="search_criteria_section" class="side_by_side_wrapper section_vertical_margins">
			
			<section id="weather_type_fieldset_wrapper" class="section_margin_right section_padding stand_out_wrapper">
				<fieldset class="fieldset weather_type_fieldset">
					<legend class="legend full_width"><span class="section_heading nowrap horizontal_center_text inline_block_box fancy_underlined heading_bottom_margin">Velg v&aelig;r:</legend>
					
					<div class="weather_type_input_wrapper column_wrapper">
						<% for(WeatherTypeView weatherType : weatherTypes) { %>
							<% String checkedAttr = (weatherType.isChecked()) ? " checked=\"checked\"": ""; %>
							<input id="<%= weatherType.getTokenLabel() %>" <%= checkedAttr %>
								type="radio" 
								name="weather_type" 
								data-weather-type-input="<%= weatherType.getTokenLabel() %>" 
								value="<%= weatherType.getTokenLabel() %>"> 
							<label for="<%= weatherType.getTokenLabel() %>" data-weather-type="<%= weatherType.getTokenLabel() %>">
								<div class="weather_type_wrapper">
									<span class="icon"><%= weatherType.getUnicodeIcon() %></span>
									<span class="label"><%= weatherType.getLabel() %></span>
								</div>
							</label>
						<% } %>
					</div>
				</fieldset>
			</section>
			
			<section class="section_margin_left section_padding stand_out_wrapper">
				<fieldset class="fieldset">
					<legend class="legend full_width"><span class="section_heading inline_block_box fancy_underlined heading_bottom_margin">Velg omr&aring;de:</span></legend>					
					
					<section class="side_by_side_wrapper">
						<div class="section_margin_right">
							<label><del>Skriv inn kommune: </del></label>
							<input id="" type="text" name="" disabled="disabled" list="municipalities_datalist" />
							
							<datalist id="municipalities_datalist">
							<%
								for (MunicipalityModel municipality : municipalities) {
									%><option value="<%= municipality.getLabel(true) %>" /><%
								}
							%>
							</datalist>
						</div>
						<aside class="section_margin_left info_box"><del>Du kan finne de b&aring;de ved &aring; skrive kommunenavnet inn i feltet eller finne de i fylkeslisten.</del></aside>
					</section>
					
					<section>
						<h2>Vis kommuner i fylke: </h2>
						
						<div class="county_tab_label_wrapper">
							<% for (CountyModel county : counties) { %>
								<label class="county_tab_label" for="county_input_<%= county.getTokenLabel(true) %>" data-county-tab-label="<%= county.getTokenLabel() %>">
									<%= county.getLabel(true) %>
								</label>
							<% } %>
						</div>
					</section>
					
					
					<section class="county_preview_wrapper">
						<% for (CountyModel county : counties) { %>
							<input id="county_input_<%= county.getTokenLabel(true) %>" class="county_preview_input" name="county_preview_input" type="radio" />
							<section class="county_preview">
								<h3><%= county.getLabel(true) %></h3>
								
								<div>
									<input disable="disabled" type="checkbox" value="${county.getTokenLabel(true)}" /> 
									<label><del>Merk alle kommuner</del></label></del>
								</div>
								
								<div class="municipality_selection_wrapper">
									<%
									for(MunicipalityModel municipality : county.getMunicipalities()) {
										%>
										<div>
											<input id="muni_<%= municipality.getCode() %>" type="checkbox" name="municipalities" data-municipality-input="<%= municipality.getCode() %>" value="<%= municipality.getCode() %>" />
											<label for="muni_<%= municipality.getCode() %>" ><%= municipality.getLabel(true) %></label>
										</div>
										<%
									}
									%>
								</div>
								
							</section>
							<%
						}
					%>
					</section>
				</fieldset>
			</section>
			
		</section>
	</form>
	
	
	<% if(!foundCabins.isEmpty()) { %>
		<jsp:include page="foundCabins.jsp" />
	<% } %>
	
				
</div>

<jsp:include page="footer.jsp" />

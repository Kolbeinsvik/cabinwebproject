<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="no.uib.info216.cabinwebproject.model.*"%>
<%@page import="no.uib.info216.cabinwebproject.view.*"%>


<jsp:include page="header.jsp" />


<div id="search" class="activity_wrapper">
	
	<article>
		<header class="stand_out_wrapper section_padding">
			<h1 class="fancy_underlined heading_bottom_margin">Semantikk</h1>
			
			<aside class="section_vertical_margins">
				<p>Dette prosjektet gjør bruk av forskjellige semantiske teknologier. Tanken bak prosjektet er å 
				gjøre data tilgjengelig for brukeren på en bedre måte ved bruk av semantiske teknologier. Under finner
				du en oversikt over hvilke vokabularer som er benyttet i prosjektet. Til høyre finner du et SPARQL
				endepunkt hvor du kan kjøre egne spørringer. </p>
			</aside>
		</header>
		
		<div class="space_between_wrapper">
			<article class="stand_out_wrapper section_padding section_vertical_margins section_margin_right">
				<h2 class="fancy_underlined heading_bottom_margin">Semantisk koblingskart</h2>
				<aside class="section_margin_bottom">
					<p>Her er en oversikt over de forskjellige vokabularene som er brukt i prosjektet.</p>
				</aside>
				
				
				<section class="section_vertical_margins">
					<h3 class="fancy_underlined heading_bottom_margin">Vokabular og prefikser</h3>
					
					<table class=" full_width">
						<thead>
							<tr>
								<th>Prefiks</th>
								<th>Vokabular</th>
								<th>Nettsted</th>
							</tr>
						</thead>
						
						<tbody>
							<!-- <tr>
								<th>SKOS</th>
								<td>Super Kul Ontologi, Sant?</td>
								<td>kul.no</td>
							</tr>
							<tr>
								<th>RDFS</th>
								<td>Radical Dangerous Furious Salamanko</td>
								<td>nope.no</td>
							</tr>-->
						</tbody>
					</table>
				</section>
				
				
				<section class="section_vertical_margins">
					<h3 class="fancy_underlined heading_bottom_margin">Vokabular og deres prefikser</h3>
					
					<table class="full_width">
						<thead>
							<tr>
								<th>Property</th>
								<th>Expected type / value</th>
							</tr>
						</thead>
						
						<tbody>
							<!-- <tr>
								<td>skos:hei</td>
								<td>String</td>
							</tr>
							<tr>
								<td>rdfs:label</td>
								<td>String</td>
							</tr>-->
						</tbody>
					</table>
				</section>
				
			</article> <%-- Semantic map --%>
			
			<article class="stand_out_wrapper section_padding section_vertical_margins section_margin_left">
				<h2 class="fancy_underlined heading_bottom_margin">SPARQL endepunkt</h2>
				
				<section>
					<ul>
						<li>@prefix <span class="vocabular_prefix">cabin:</span> &lt;<span class="vocabular_uri">http://uib.no/info216/cabinProject/</em>&gt; .</li>
						<li>@prefix <span class="vocabular_prefix">cabinRes:</span> &lt;<span class="vocabular_uri">http://uib.no/info216/cabinProject/resource/</em>&gt; .</li>
						<li>@prefix <span class="vocabular_prefix">cabinInd:</span> &lt;<span class="vocabular_uri">http://uib.no/info216/cabinProject/individual/</em>&gt; .</li>
						<li>@prefix <span class="vocabular_prefix">dbRes:</span> &lt;<span class="vocabular_uri">http://dbpedia.org/resource/</em>&gt; .</li>
						<li>@prefix <span class="vocabular_prefix">dbOwl:</span> &lt;<span class="vocabular_uri">http://dbpedia.org/ontology/</em>&gt; .</li>
						<li>@prefix <span class="vocabular_prefix">geo:</span> &lt;<span class="vocabular_uri">http://www.w3.org/2003/01/geo/wgs84_pos#</em>&gt; .</li>
						<li>@prefix <span class="vocabular_prefix">schema:</span> &lt;<span class="vocabular_uri">http://schema.org/</em>&gt; .</li>
					</ul>
					
					<textarea></textarea>
				</section>
			</article>
		</div>
		
	</article>
	
</div>

<jsp:include page="footer.jsp" />

package no.uib.info216.cabinwebproject.model;

import no.uib.info216.cabinwebproject.util.StringUtil;

public class SemanticCabinModel {
	
	private String label;
	private String utUrl;

	private String imagePath = "image/cabin.jpg";

	private float precipitation;
	private float temperature;
	
	private WeatherModel weather;
	
	private String county;
	private String municipality;
	
	
//	public SemanticCabinModel(String label, String utUrl, WeatherTypeModel weatherType, float rain, float temperature,
//			String county, String municipality) {
//		this.label = label;
//		this.utUrl = utUrl;
//		
//		this.weatherType = weatherType;
//		this.precipitation = rain;
//		this.temperature = temperature;
//		
//		this.county = county;
//		this.municipality = municipality;
//	}
	
	public SemanticCabinModel(String label, String utUrl, String county, String municipality, WeatherModel weather) {
		this.label = label;
		this.utUrl = utUrl;
		
		this.county = county;
		this.municipality = municipality;
		
		this.weather = weather;
	}
	

	public String getLabel() {
		return label;
	}
	
	public String getEscapedLabel() {
		return StringUtil.escapeHtml(getLabel());
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUtUrl() {
		return utUrl;
	}

	public void setUtUrl(String utUrl) {
		this.utUrl = utUrl;
	}

//	public WeatherTypeModel getWeatherType() {
//		return weatherType;
//	}
//
//	public void setWeatherType(WeatherTypeModel weatherType) {
//		this.weatherType = weatherType;
//	}

	/**
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}
	
	public String getEscapedCounty() {
		return StringUtil.escapeHtml(getCounty());
	}

	/**
	 * @param county the county to set
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * @return the municipality
	 */
	public String getMunicipality() {
		return municipality;
	}
	
	public String getEscapedMunicipality() {
		return StringUtil.escapeHtml(getMunicipality());
	}

	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

//	@Override
//	public String toString() {
//		return "SemanticCabinModel [label=" + label + ", utUrl=" + utUrl
//				+ ", weatherType=" + weatherType + ", precipitation="
//				+ precipitation + ", temperature=" + temperature + ", county="
//				+ county + ", municipality=" + municipality + "]";
//	}


	/**
	 * @return the weather
	 */
	public WeatherModel getWeather() {
		return weather;
	}


	/**
	 * @param weather the weather to set
	 */
	public void setWeather(WeatherModel weather) {
		this.weather = weather;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SemanticCabinModel [label=" + label + ", utUrl=" + utUrl
				+ ", precipitation=" + precipitation + ", temperature="
				+ temperature + ", weather=" + weather + ", county=" + county
				+ ", municipality=" + municipality + "]";
	}


	public String getImagePath() {
		return imagePath;
	}


	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
}

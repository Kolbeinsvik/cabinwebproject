package no.uib.info216.cabinwebproject.model;

import java.text.DecimalFormat;

import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

public class CabinModel {

	private String label;
	private GeoPointModel geoPoint;
	private String utUrl;
	private String imagePath = "image/cabin.jpg";
	

	public CabinModel(String label, GeoPointModel point, String utUrl) {
		this.label = label;
		this.geoPoint = point;
		this.utUrl = utUrl;
	}
	

	public CabinModel(String label, double longitude, double latitude, String url) {
		this(label, new GeoPointModel(latitude, longitude), url);
	}
	

	public String getLabel() {
		return label;
	}
	

	public String getTokenLabel(boolean uppercaseFirstLetter) {
		return StringUtil.strTokenizer(getLabel(), uppercaseFirstLetter, true);
	}
	


	public String getTokenLabel() {
		return getTokenLabel(false);
	}
	


	public void setLabel(String label) {
		this.label = label;
	}


	public GeoPointModel getGeoPoint() {
		return geoPoint;
	}


	public void setGeoPoint(GeoPointModel geoPoint) {
		this.geoPoint = geoPoint;
	}


	public String getUtUrl() {
		return utUrl;
	}


	public void setUtUrl(String url) {
		this.utUrl = url;
	}


	public boolean isGeoEqual(double externalLatitude, double externalLongitude) {
		DecimalFormat geoFormat = new DecimalFormat("##.##");
		boolean isEqual = false;
		
		if(geoFormat.format(geoPoint.getLatitude()).equals(geoFormat.format(externalLatitude)) && 
				geoFormat.format(geoPoint.getLongitude()).equals(geoFormat.format(externalLongitude))) {
			isEqual = true;
		}
		
		return isEqual;
	}
	
	
	public String getSemanticUri() {
		String token = StringUtil.semanticTokenizer(getLabel(), true);
		String uri = SemanticSpecs.PROJECT_CABIN_RES_BASE + token;
		return uri;
	}


	public String getImagePath() {
		return imagePath;
	}


	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}

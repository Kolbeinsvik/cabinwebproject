package no.uib.info216.cabinwebproject.model;

import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

public class WeatherModel {

	private PlaceModel place;
	private String isoDate;
	private float precipitation;
	private float temperature;
	
	public WeatherModel(PlaceModel place, String isoDate, float precipitation, float temperature) {
		this.place = place;
		this.isoDate = isoDate;
		this.precipitation = precipitation;
		this.temperature = temperature;
	}

	/**
	 * @return the place
	 */
	public PlaceModel getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(PlaceModel place) {
		this.place = place;
	}

	/**
	 * @return the isoDate
	 */
	public String getIsoDate() {
		return isoDate;
	}

	/**
	 * @param isoDate the isoDate to set
	 */
	public void setIsoDate(String isoDate) {
		this.isoDate = isoDate;
	}

	/**
	 * @return the precipitation
	 */
	public float getPrecipitation() {
		return precipitation;
	}

	/**
	 * @param precipitation the precipitation to set
	 */
	public void setPrecipitation(float precipitation) {
		this.precipitation = precipitation;
	}

	/**
	 * @return the temperature
	 */
	public float getTemperature() {
		return temperature;
	}

	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	
	

	public String getSemanticUri() {
		String token = StringUtil.semanticTokenizer(place.getPlaceName(), true);
		String uri = SemanticSpecs.PROJECT_AREA_RES_BASE + isoDate + "/" + token;
		return uri;
	}
	
	
	/**
	 * Simple method for finding weathertype based on precipitation and temperature
	 * @param precipitation
	 * @param temperature
	 * @return
	 */
	public String findWeatherType(){
		return findWeatherType(precipitation, temperature);
	}
	
	
	/**
	 * Simple method for finding weathertype based on precipitation and temperature
	 * @param precipitation
	 * @param temperature
	 * @return
	 */
	public static String findWeatherType(float precipitation, float temperature){
		if(precipitation <= 0) {
			return "Sun";
		} else if(precipitation > 0 && temperature >= 0) {
			return "Rain";
		} else if(precipitation > 0 && temperature < 0) {
			return "Snow";
		} else {
			return "Overcast";
		}
	}
	
	
	
}

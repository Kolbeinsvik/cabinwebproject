package no.uib.info216.cabinwebproject.model;

import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

public abstract class AbstractAreaModel {

	private String label;
	
	public AbstractAreaModel(String label) {
		this.label = label;
	}
	

	public String getLabel() {
		return getLabel(false);
	}
	


	public String getLabel(boolean convertToHtmlEntities) {
		String localLabel = label;
		if(convertToHtmlEntities) {
			localLabel = StringUtil.escapeHtml(localLabel);
		}
		return localLabel;
	}

	public String getTokenLabel(boolean convertToHtmlEntities) {
		return getLabel(convertToHtmlEntities).toLowerCase();
	}

	public String getTokenLabel() {
		return getTokenLabel(false);
	}
	
	public String getSemanticLabel() {
		return StringUtil.strTokenizer(getLabel(), true);
	}
	

	public void setLabel(String label) {
		this.label = label;
	}


	/**
	 * @return the semanticUri
	 */
	public String getSemanticUri() {
		String token = StringUtil.semanticTokenizer(getLabel(), true);
		String uri = SemanticSpecs.PROJECT_AREA_RES_BASE + token;
		return uri;
	}


	@Override
	public String toString() {
		return "AbstractAreaModel [label=" + label + "]";
	}
}

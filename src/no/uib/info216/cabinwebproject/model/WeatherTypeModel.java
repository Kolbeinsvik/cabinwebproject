package no.uib.info216.cabinwebproject.model;

import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.rdf.model.Resource;

public class WeatherTypeModel {

	private String tokenLabel;
	private String label;
	
	private float precipitation;
	private float temperature;
	
	private String type;
	

	public WeatherTypeModel(String tokenLabel, String label) {
		this.tokenLabel = tokenLabel;
		this.label = label;
	}

	public WeatherTypeModel(String tokenLabel) {
		this(tokenLabel, retrieveLabel(tokenLabel));
	}


	private static String retrieveLabel(String tokenLabel) {
		String label;
		
		switch(tokenLabel) {
		case "sun":
			label = "Sol"; 			break;
		case "overcast":
			label = "Overskyet"; 	break;
		case "rain":
			label = "Regn"; 		break;
		case "snow":
			label = "Sn&oslasj;"; 	break;
		default: 
			label = "Unrecognized"; break;
		}
		
		return label;
	}


	/**
	 * @return the tokenLabel
	 */
	public String getTokenLabel() {
		return tokenLabel;
	}


	/**
	 * @param tokenLabel the tokenLabel to set
	 */
	public void setTokenLabel(String tokenLabel) {
		this.tokenLabel = tokenLabel;
	}


	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}


	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}





	public float getPrecipitation() {
		return precipitation;
	}





	public void setPrecipitation(float precipitation) {
		this.precipitation = precipitation;
	}





	public float getTemperature() {
		return temperature;
	}





	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	
	
	
	public static Resource getResource(WeatherTypeModel weatherType) {
		Resource res = null;
		
		switch(weatherType.getTokenLabel()) {
			case "sun": res = SemanticSpecs.WEATHER_SUN_TYPE; break;
			case "overcast": res = SemanticSpecs.WEATHER_OVERCAST_TYPE; break;
			case "rain": res = SemanticSpecs.WEATHER_RAIN_TYPE; break;
			case "snow": res = SemanticSpecs.WEATHER_SNOW_TYPE; break;
		}
		
		return res;
	}

	
	public String getTypeUri() {
		String tokenType = tokenLabel;
		tokenType = tokenType.substring(0, 1).toUpperCase() +  tokenType.substring(1);
		
		return SemanticSpecs.PROJECT_WEATHER_RES_BASE + "Weather_" + tokenType;
	}



}

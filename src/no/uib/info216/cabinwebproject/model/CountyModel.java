package no.uib.info216.cabinwebproject.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CountyModel extends AbstractAreaModel {
	private List<MunicipalityModel> municipalities;
	
	public CountyModel(String label) {
		super(label);
		municipalities = new ArrayList<MunicipalityModel>();
	}
	
	public void add(MunicipalityModel municipality) {
		municipalities.add(municipality);
	}
	
	public Iterator<MunicipalityModel> iterator() {
		return municipalities.iterator();
	}

	public List<MunicipalityModel> getMunicipalities() {
		return municipalities;
	}

	public void setMunicipalities(List<MunicipalityModel> municipalities) {
		this.municipalities = municipalities;
	}

	@Override
	public String toString() {
		return "CountyModel [label=" + super.getLabel() + ", municipalities="
				+ municipalities + "]";
	}
}

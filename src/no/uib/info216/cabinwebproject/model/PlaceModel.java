package no.uib.info216.cabinwebproject.model;

import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.SemanticUtil;
import no.uib.info216.cabinwebproject.util.StringUtil;

public class PlaceModel {

	private String placeName;
	private String placeType;
	private MunicipalityModel municipality;
	private GeoPointModel geoPoint;
	private String forecastUrl;
	
	
	/**
	 * 
	 * @param placeName
	 * @param placeType
	 * @param geoPoint
	 * @param forecastUrl
	 */
	public PlaceModel(String placeName, String placeType, MunicipalityModel municipality, GeoPointModel geoPoint, String forecastUrl) {
		this.placeName = placeName;
		this.placeType = placeType;
		this.municipality = municipality;
		this.geoPoint = geoPoint;
		this.forecastUrl = forecastUrl;
	}


	/**
	 * @return the placeName
	 */
	public String getPlaceName() {
		return placeName;
	}


	/**
	 * @param placeName the placeName to set
	 */
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}


	/**
	 * @return the placeType
	 */
	public String getPlaceType() {
		return placeType;
	}


	/**
	 * @param placeType the placeType to set
	 */
	public void setPlaceType(String placeType) {
		this.placeType = placeType;
	}


	/**
	 * @return the geoPoint
	 */
	public GeoPointModel getGeoPoint() {
		return geoPoint;
	}


	/**
	 * @param geoPoint the geoPoint to set
	 */
	public void setGeoPoint(GeoPointModel geoPoint) {
		this.geoPoint = geoPoint;
	}


	/**
	 * @return the forecastUrl
	 */
	public String getForecastUrl() {
		return forecastUrl;
	}


	/**
	 * @param forecastUrl the forecastUrl to set
	 */
	public void setForecastUrl(String forecastUrl) {
		this.forecastUrl = forecastUrl;
	}
	
	
	public String getSemanticUri() {
		String token = StringUtil.semanticTokenizer(getPlaceName(), true);
		String uri = SemanticSpecs.PROJECT_AREA_RES_BASE + token;
		return uri;
	}


	/**
	 * @return the municipality
	 */
	public MunicipalityModel getMunicipality() {
		return municipality;
	}


	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(MunicipalityModel municipality) {
		this.municipality = municipality;
	}
	


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AltPlaceModel [placeName=" + placeName + ", placeType="
				+ placeType + ", municipality=" + municipality + ", geoPoint="
				+ geoPoint + ", forecastUrl=" + forecastUrl + "]";
	}
	
}

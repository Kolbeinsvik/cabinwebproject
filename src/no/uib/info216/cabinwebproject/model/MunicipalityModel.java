package no.uib.info216.cabinwebproject.model;

import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

public class MunicipalityModel extends AbstractAreaModel {
	private int code;
	private String semanticUri;
	
	public MunicipalityModel(int code, String label) {
		super(label);
		this.code = code;
		this.semanticUri = "";
	}
	
	public MunicipalityModel(String label) {
		this(-1, label);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return super.toString() + " MunicipalityModel [code=" + code + "]";
	}

	/**
	 * @return the semanticUri
	 */
	public String getSemanticUri() {
		String uri = (semanticUri.equalsIgnoreCase("")) ? super.getSemanticUri(): semanticUri;
		return uri;
	}

	/**
	 * @param semanticUri the semanticUri to set
	 */
	public void setSemanticUri(String semanticUri) {
		this.semanticUri = semanticUri;
	}

	
}

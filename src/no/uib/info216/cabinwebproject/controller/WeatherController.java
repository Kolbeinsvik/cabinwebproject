package no.uib.info216.cabinwebproject.controller;

import no.uib.info216.cabinwebproject.lifter.WeatherLifter;
import no.uib.info216.cabinwebproject.parser.WeatherParser;
import no.uib.info216.cabinwebproject.query.DatasetQuery;
import no.uib.info216.cabinwebproject.query.WeatherDatasetQuery;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.SemanticUtil;

public class WeatherController {

	/**
	 * Parse and lift data concerning weather
	 */
	public static void buildWeatherData() {
		//Check if there are weather data in the TDB
		if(!updatedTdbWeatherDataExists()) {
			MainController.debugMsg("weather", "Could not find any weather data in the TDB. Commence parsing and lifting.");

			// parse and lift weather data into the TDB
			WeatherParser.parseWeatherData();
			WeatherLifter.liftWeatherData();
			MainController.debugMsg("weather", "Operation 'weather parse and lift' was a success. ");
		}
	}


	/**
	 * Returns whether weatherdata in tdb exist and is updated/current
	 */
	public static boolean updatedTdbWeatherDataExists() {
		boolean weatherDataExists = DatasetQuery.anyOfTypeExists(SemanticSpecs.WEATHER_TYPE);
		boolean currentWeatherData = WeatherDatasetQuery.hasCurrentWeatherData();

		boolean updated = (weatherDataExists && currentWeatherData) ? true: false;
		return updated;
	}


	/**
	 * Prints all weathertypes and corresponding cabincount of said type to console
	 */
	public static void testWeatherData() {
		int weatherCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.WEATHER_TYPE));
		int sunCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.WEATHER_SUN_TYPE));
		int overcastCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.WEATHER_OVERCAST_TYPE));
		int rainCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.WEATHER_RAIN_TYPE));
		int snowCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.WEATHER_SNOW_TYPE));

		MainController.debugMsg("testing", ""
				+ "Total weather: " + weatherCount + ", "
				+ "Sun weather: " + sunCount + ", "
				+ "Overcast weather: " + overcastCount + ", "
				+ "Rain weather: " + rainCount + ", "
				+ "Snow weather: " + snowCount);


		//		MainController.printData(SemanticSpecs.WEATHER_TYPE, "Total weather");
	}


}

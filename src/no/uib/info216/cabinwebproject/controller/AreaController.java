package no.uib.info216.cabinwebproject.controller;

import no.uib.info216.cabinwebproject.lifter.AreaLifter;
import no.uib.info216.cabinwebproject.parser.AreaParser;
import no.uib.info216.cabinwebproject.query.DatasetQuery;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.SemanticUtil;

public class AreaController {


	/**
	 * Parse areas (municipalities and counties) from file and semantically lift data if these are not found in TDB
	 */
	public static void buildAreaData(boolean useSamplePlaces) {
		if(!tdbAreaExists()) {
			MainController.debugMsg("areas", "Could not find any areas in the tdb. Commence parsing and lifting.");

			//Parse and lift counties and municipalities
			AreaParser.parseAreas(useSamplePlaces);
			AreaLifter.liftAreas();
			AreaLifter.liftPlaceData();

			MainController.debugMsg("areas", "Operation 'area parse and lift' was a success. ");
		}
	}


	/**
	 * Checks if there are municipality individuals in the TDB
	 * @return true if a municipality exists, false if not
	 */
	public static boolean tdbAreaExists() {
		boolean countyExists = DatasetQuery.anyOfTypeExists(SemanticSpecs.AREA_COUNTY_TYPE);
		boolean muniExists = DatasetQuery.anyOfTypeExists(SemanticSpecs.AREA_MUNICIPALITY_TYPE);
		boolean placeDataExists = DatasetQuery.anyOfTypeExists(SemanticSpecs.PLACE_TYPE);

		boolean allExists = (countyExists && muniExists && placeDataExists) ? true: false;
		return allExists;
	}


	/**
	 * Retrieves the number of <em>counties</em> and <em>municipalities</em>
	 * from the TDB and prints them to the terminal
	 */
	public static void testAreaData() {
		int countyCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.AREA_COUNTY_TYPE));
		int muniCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.AREA_MUNICIPALITY_TYPE));
		int placeCount = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.PLACE_TYPE));

		MainController.debugMsg("testing", ""
				+ "Counties: " + countyCount + ", "
				+ "Municipalities: " + muniCount + ", "
				+ "Places: " + placeCount);

	}

}

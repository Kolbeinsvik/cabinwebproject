/**
 * 
 */
package no.uib.info216.cabinwebproject.controller;

/**
 * Main class of the project. Runs the MainController-class
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MainController controller = new MainController();
//		controller.setResetTdbOnStartFlag(false);
//		controller.setParseSampleSizedWeatherDataFlag(false);
		controller.init();
	}

}

package no.uib.info216.cabinwebproject.controller;

import java.io.File;

import no.uib.info216.cabinwebproject.query.DatasetQuery;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.SharedData;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;

public class MainController {

	private boolean resetTdbOnStartFlag;
	private boolean parseSampleSizedWeatherDataFlag;

	/**
	 * The Main Controller of the project. Responsible for initiating all processes
	 */
	public MainController() {
		this.resetTdbOnStartFlag = false;
		this.parseSampleSizedWeatherDataFlag = false;
	}

	/**
	 * Builds filestructure and dataset
	 */
	public void init() {
		fileStructureSetup();

		buildDataset();

		SemanticSpecs.TDB_DATASET.close();
		debugMsg("testing", "Done.");
	}



	/**
	 * Validates/Creates folder structure
	 */
	public void fileStructureSetup() {
		File tdbWrapperDirectory = new File(SharedData.getInstance().getTdbWrapperPath());
		if(!tdbWrapperDirectory.isDirectory()) {
			tdbWrapperDirectory.mkdir();
		}

		if(this.resetTdbOnStartFlag) {
			resetDataFiles();
		}
	}

	/**
	 * Remove all external data-files and create 'fresh' files
	 */
	public static void resetDataFiles() {
		SharedData sharedData = SharedData.getInstance();
		// files
		File tdbDir = new File(sharedData.getTdbPath());
		File weatherPlacesTmpFile = new File(sharedData.getWeatherPlaceTmpPath());
		File weatherDaoTmpFile = new File(sharedData.getWeatherPlaceDaoTmpPath());

		if(tdbDir.exists() && tdbDir.isDirectory()) {
			// remove tdb
			for(File tdbFile : tdbDir.listFiles()) {
				tdbFile.delete();
			}
			tdbDir.delete();
		}
		tdbDir.delete();


		// remove temp files
		if(weatherPlacesTmpFile.exists() && weatherPlacesTmpFile.isFile()) {
			weatherPlacesTmpFile.delete();
		}
		if(weatherDaoTmpFile.exists() && weatherDaoTmpFile.isFile()) {
			weatherDaoTmpFile.delete();
		}

		debugMsg("Files", "TDB and temp files reset on startup");
	}


	/**
	 * Adds Ontologies, builds area- weather- and cabin-data and make testqueries
	 */
	public void buildDataset() {
		SemanticSpecs.addOnt();

		AreaController.buildAreaData(this.parseSampleSizedWeatherDataFlag);
		WeatherController.buildWeatherData();
		CabinController.buildCabinData();

		AreaController.testAreaData();
		WeatherController.testWeatherData();
		CabinController.testCabinData();

	}

	/**
	 * Default project method for printing to console
	 * @param msg Message to be printed to console
	 */
	public static void debugMsg(String msg) {
		debugMsg("generic", msg);
	}

	/**
	 * Default project method for printing to console
	 * @param msg Message to be printed to console.
	 * @param subject Subject of console message (appears in uppercase)
	 */
	public static void debugMsg(String subject, String msg) {
		System.out.println("[DEBUG " + subject.toUpperCase() + "] " + msg);
	}


	/**
	 * Prints resource and label to console
	 */
	public static void printData(Resource res, String printLabel) {
		ResultSet results = DatasetQuery.getAllOfType(res);
		while(results.hasNext()) {
			QuerySolution solution = results.next();
			MainController.debugMsg("testing", printLabel + ": " + solution);
		}
	}

	public boolean isResetTdbOnStartFlag() {
		return this.resetTdbOnStartFlag;
	}

	public void setResetTdbOnStartFlag(boolean resetTdbOnStartFlag) {
		this.resetTdbOnStartFlag = resetTdbOnStartFlag;
	}

	public boolean isParseSampleSizedWeatherDataFlag() {
		return this.parseSampleSizedWeatherDataFlag;
	}

	/**
	 * <string>NOTE</strong>: This is only valid when the TDB is reset!
	 * @param useSampleSizedWeatherDataFlag
	 */
	public void setParseSampleSizedWeatherDataFlag(
			boolean useSampleSizedWeatherDataFlag) {
		this.parseSampleSizedWeatherDataFlag = useSampleSizedWeatherDataFlag;
	}

}

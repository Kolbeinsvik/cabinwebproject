package no.uib.info216.cabinwebproject.controller;

import java.io.IOException;

import no.uib.info216.cabinwebproject.lifter.CabinLifter;
import no.uib.info216.cabinwebproject.parser.CabinParser;
import no.uib.info216.cabinwebproject.query.DatasetQuery;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.SemanticUtil;

/**
 * Class responsible for opperations concerning Cabin-data and Area-data,
 * like building dataset and running semantic lifting-opperations
 */
public class CabinController {

	/**
	 * Parse cabins from file and semantically lift data if these are not found in TDB
	 */
	public static void buildCabinData() {

		//Check if cabins exist in tdb
		if(!tdbCabinsExists()) {
			MainController.debugMsg("cabins", "Could not find any cabins in the tdb. Commence parsing and lifting.");

			//Parse and lift cabin data to tdb
			try {
				CabinParser.parseCabins();
				CabinLifter.liftCabins();
				MainController.debugMsg("cabins", "Operation 'cabins parse and lift' was a success. ");
			} catch (IOException e) {
				System.err.println("Cabin resource file does not exists / could not find. ");
				e.printStackTrace();
			}
		}
	}


	/**
	 * Checks if there exists cabins in the tdb by querying a specific one.
	 * @return true if there are cabins; false if not
	 */
	public static boolean tdbCabinsExists() {
		boolean exists = DatasetQuery.anyOfTypeExists(SemanticSpecs.CABIN_TYPE);
		return exists;
	}


	/**
	 * Writes out number of cabins
	 */
	public static void testCabinData() {
		int count = SemanticUtil.parseLiteral("count", DatasetQuery.getCount("count", SemanticSpecs.CABIN_TYPE));
		MainController.debugMsg("testing", "Cabins: " + count);

		//		MainController.printData(SemanticSpecs.CABIN_TYPE, "Cabins");
	}


}

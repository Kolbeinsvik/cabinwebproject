package no.uib.info216.cabinwebproject.view;

import java.util.ArrayList;
import java.util.List;

public class HeaderView {
	
	private List<NavItem> primaryNav;
	
	public HeaderView(String contextPath) {
		primaryNav = new ArrayList<NavItem>();
		primaryNav.add(new NavItem(contextPath + "/", "Hyttesøk"));
		primaryNav.add(new NavItem(contextPath + "/about", "Om nettstedet"));
		primaryNav.add(new NavItem(contextPath + "/semantic", "Semantikk"));
	}

	/**
	 * @return the primaryNav
	 */
	public List<NavItem> getPrimaryNav() {
		return primaryNav;
	}

	/**
	 * @param primaryNav the primaryNav to set
	 */
	public void setPrimaryNav(List<NavItem> primaryNav) {
		this.primaryNav = primaryNav;
	}
}

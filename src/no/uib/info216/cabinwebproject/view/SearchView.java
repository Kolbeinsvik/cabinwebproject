/**
 * 
 */
package no.uib.info216.cabinwebproject.view;

import java.util.ArrayList;
import java.util.List;

import no.uib.info216.cabinwebproject.dao.CabinDao;
import no.uib.info216.cabinwebproject.model.CabinModel;
import no.uib.info216.cabinwebproject.model.MunicipalityModel;
import no.uib.info216.cabinwebproject.model.SemanticCabinModel;
import no.uib.info216.cabinwebproject.model.WeatherTypeModel;

/**
 *
 */
public class SearchView {
	
	private List<WeatherTypeView> weatherTypes;
	
	
	public SearchView() {
		weatherTypes = new ArrayList<WeatherTypeView>();
		weatherTypes.add(new WeatherTypeView("sun", "Sol", WeatherTypeView.SUN_ICON, true));
		weatherTypes.add(new WeatherTypeView("overcast", "Overskyet", WeatherTypeView.OVERCAST_ICON));
		weatherTypes.add(new WeatherTypeView("rain", "Regn", WeatherTypeView.RAIN_ICON));
		weatherTypes.add(new WeatherTypeView("snow", "Sn&oslash;", WeatherTypeView.SNOW_ICON));
	}
	
	
	public static List<SemanticCabinModel> getFoundCabins(WeatherTypeModel weatherType, List<MunicipalityModel> municipalities) {
		List<SemanticCabinModel> cabins = CabinDao.getCabins(weatherType, municipalities);
		
		return cabins;
	}
	

	/**
	 * @return the weatherTypes
	 */
	public List<WeatherTypeView> getWeatherTypes() {
		return weatherTypes;
	}

	
	/**
	 * @param weatherTypes the weatherTypes to set
	 */
	public void setWeatherTypes(List<WeatherTypeView> weatherTypes) {
		this.weatherTypes = weatherTypes;
	}
	
}

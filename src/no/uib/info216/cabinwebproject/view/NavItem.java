package no.uib.info216.cabinwebproject.view;

public class NavItem {
	
	private String href;
	private String label;
	private boolean current;
	
	public NavItem(String href, String label) {
		this.href = href;
		this.label = label;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the current
	 */
	public boolean isCurrent() {
		return current;
	}

	/**
	 * @param current the current to set
	 */
	public void setCurrent(boolean current) {
		this.current = current;
	}
}

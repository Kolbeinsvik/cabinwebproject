package no.uib.info216.cabinwebproject.view;

import no.uib.info216.cabinwebproject.model.WeatherTypeModel;

public class WeatherTypeView extends WeatherTypeModel {

	public static final int SUN_ICON = 0;
	public static final int OVERCAST_ICON = 1;
	public static final int RAIN_ICON = 2;
	public static final int SNOW_ICON = 3;
	public static final int THUNDER_ICON = 4;

	private static final String[] unicodeIcons = {"&#9728;", "&#9729;", "&#9730;", "&#10052;", "&#9928;"}; // alt. overcast: &#9925;

	@SuppressWarnings("unused")
	private String tokenLabel;
	@SuppressWarnings("unused")
	private String label;
	private int iconIndex;
	private boolean checked;


	public WeatherTypeView(String tokenLabel, String label, int icon, boolean checked) {
		super(tokenLabel, label);
		this.iconIndex = icon;
		this.checked = checked;
	}


	public WeatherTypeView(String tokenLabel, String label, int icon) {
		this(tokenLabel, label, icon, false);
	}


	public String getUnicodeIcon() {
		return WeatherTypeView.unicodeIcons[this.iconIndex];
	}


	public static String getUnicodeIcon(String token) {
		int index = 0;

		switch(token) {
		case "sun":
			index = 0; break;
		case "overcast":
			index = 1; break;
		case "rain":
			index = 2; break;
		case "snow":
			index = 3; break;
		}

		return WeatherTypeView.unicodeIcons[index];
	}


	/**
	 * @return the icon
	 */
	public int getIconIndex() {
		return this.iconIndex;
	}


	/**
	 * @param icon the icon to set
	 */
	public void setIconIndex(int index) {
		this.iconIndex = index;
	}


	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return this.checked;
	}


	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}


}

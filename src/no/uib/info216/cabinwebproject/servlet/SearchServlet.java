package no.uib.info216.cabinwebproject.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.uib.info216.cabinwebproject.dao.AreaDao;
import no.uib.info216.cabinwebproject.model.MunicipalityModel;
import no.uib.info216.cabinwebproject.model.SemanticCabinModel;
import no.uib.info216.cabinwebproject.model.WeatherTypeModel;
import no.uib.info216.cabinwebproject.view.HeaderView;
import no.uib.info216.cabinwebproject.view.SearchView;


/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/index.html")
//@WebServlet(value="", loadOnStartup=1)
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchServlet() {
		super();
	}

	//	public void init

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AreaDao areaDao = AreaDao.getInstance();
		HeaderView headerView = new HeaderView(ServletUtil.getContextPath(request.getContextPath()));
		SearchView searchView = new SearchView();
		
		
		request.setAttribute("counties", areaDao.getCounties());
		request.setAttribute("municipalities", areaDao.getMunicipalities());
		request.setAttribute("primaryNav", headerView.getPrimaryNav());
		request.setAttribute("weatherTypes", searchView.getWeatherTypes());
		request.setAttribute("weatherType", new WeatherTypeModel("sun"));
		request.setAttribute("foundCabins", new ArrayList<SemanticCabinModel>());
		request.setAttribute("hasSearched", false);
		
		
		String action = request.getParameter("action");
		String weatherType = request.getParameter("weather_type");
		String[] muniCodeStrs = request.getParameterValues("municipalities");
		
		if(action == null) action = "";
		if(weatherType == null) weatherType = "";
		if(muniCodeStrs == null) muniCodeStrs = new String[0];
		
		Integer[] muniCodes = new Integer[muniCodeStrs.length];
		
		for(int i = 0; i < muniCodeStrs.length; i++) {
			muniCodes[i] = Integer.parseInt(muniCodeStrs[i]);
		}
		
//		try {
			if (assertValidSearch(action, weatherType)) {
				doSearch(request, weatherType, muniCodes);
			}
//		} catch (Exception e) {
//			if(e.getMessage().equalsIgnoreCase("weatherType"))
//				System.err.println("Error: weathertype could not be found");
//			else 
//				e.printStackTrace();
//		}
		
		request.getRequestDispatcher("/search.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * 
	 * @param action
	 * @param weatherType
	 * @param munis Municipalities
	 */
	public void doSearch(HttpServletRequest request, String weatherTypeTokenLabel, Integer[] muniCodes) {
		WeatherTypeModel weatherType = new WeatherTypeModel(weatherTypeTokenLabel);
		List<MunicipalityModel> municipalities = (muniCodes.length > 0) ? 
				AreaDao.getMunicipalities(muniCodes): AreaDao.getMunicipalities();
		
				
		List<SemanticCabinModel> cabins = SearchView.getFoundCabins(weatherType, municipalities);
		request.setAttribute("foundCabins", cabins);
		request.setAttribute("weatherType", weatherType);
		request.setAttribute("hasSearched", true);
	}
	
	/**
	 * 
	 * @param action
	 * @param weatherType
	 * @return
	 */
	public boolean assertValidSearch(String action, String weatherType) {
		if (!action.equals("search")) {
			return false;
		}
//		if (weatherType.equals("")) {
//			throw new Exception("weatherType");
//		}
		return true;
	}

}

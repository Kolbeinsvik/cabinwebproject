package no.uib.info216.cabinwebproject.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import no.uib.info216.cabinwebproject.controller.MainController;
import no.uib.info216.cabinwebproject.parser.AreaParser;
import no.uib.info216.cabinwebproject.util.SharedData;


@WebListener()
public class MainContextListener implements ServletContextListener {
	ServletContext context;
	String projectBasePath;

	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		System.out.println("Context initiated");
		this.context = contextEvent.getServletContext();

		SharedData.getInstance().setProjectBasePath(this.context.getRealPath("/WEB-INF") + "/");

		MainController controller = new MainController();
//		controller.setResetTdbOnStartFlag(false);
//		controller.setParseSampleSizedWeatherDataFlag(false);
		controller.init();

		this.context.setAttribute("contextPath", ServletUtil.getContextPath(this.context));

		AreaParser.parseAreas();
	}

	@Override
	public void contextDestroyed(ServletContextEvent contextEvent) {
		this.context = contextEvent.getServletContext();
		System.out.println("Context destroyed");
	}

}

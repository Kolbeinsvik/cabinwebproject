package no.uib.info216.cabinwebproject.servlet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class ServletUtil {
	
	public static final String APP_NAME = "cabinwebproject";
	
	public static String getContextPath(HttpServletRequest request) {
		return getContextPath(request.getContextPath());
	}
	
	public static String getContextPath(ServletContext context) {
		return getContextPath(context.getContextPath());
	}
	
	public static String getContextPath(String contextPath) {
		if(contextPath.isEmpty()) {
			contextPath = "/" + APP_NAME;
		}
		
		return contextPath;
	}
}

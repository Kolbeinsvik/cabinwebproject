package no.uib.info216.cabinwebproject.util;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;

public class SemanticUtil {

	
	/**
	 * Takes a <em>ResultSet</em> and returns the corresponding integer value
	 * @param label The name of the <em>literal</em>
	 * @param results The <em>ResultSet</em> to be checked
	 * @return The number held by the literal, or <em>-1</em> if no such literal exists
	 */
	public static int parseLiteral(String label, ResultSet results) {
		int integer = -1;
		
		if(results.hasNext()) {
			QuerySolution solution = results.next();
			
			if(solution.contains(label) && solution.get(label).isLiteral()) { 
				integer = Integer.parseInt(solution.getLiteral(label).toString());
			}
		}
		
		return integer;
	}
	

	/**
	 * Takes a string and returns the corresponding string value
	 * @param varName The name of the <em>literal</em>
	 * @param solution The <em>solution</em> to be checked
	 * @return The string held by the literal, or <em>null</em> if no such literal exists
	 */
	public static String parseLiteralAsString(String varName, QuerySolution solution) {
		String str = null;
		
		if(solution.contains(varName) && solution.get(varName).isLiteral()) { 
			str = solution.getLiteral(varName).toString();
		}
		
		return str;
	}


	/**
	 * Takes a string and returns the corresponding integer value
	 * @param varName The name of the <em>literal</em>
	 * @param solution The <em>solution</em> to be checked
	 * @return The integer held by the literal, or <em>-1</em> if no such literal exists
	 */
	public static int parseLiteralAsInteger(String varName, QuerySolution solution) {
		int intValue = -1;
		
		if(solution.contains(varName) && solution.get(varName).isLiteral()) { 
			intValue = Integer.parseInt(solution.getLiteral(varName).toString());
		}
		
		return intValue;
	}
	


	/**
	 * Takes a string and returns the corresponding float value
	 * @param varName The name of the <em>literal</em>
	 * @param solution The <em>solution</em> to be checked
	 * @return The float value held by the literal, or <em>-1</em> if no such literal exists
	 */
	public static float parseLiteralToFloat(String varName, QuerySolution solution) {
		float flt = -1;
		
		if(solution.contains(varName) && solution.get(varName).isLiteral()) { 
			flt = Float.parseFloat(solution.getLiteral(varName).toString());
		}
		
		return flt;
	}
	
}

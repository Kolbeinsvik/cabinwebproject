package no.uib.info216.cabinwebproject.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDBFactory;

public class SemanticSpecs {

	// TDB objects
	public static final Dataset TDB_DATASET = getDataset();
	public static final Model TDB_MODEL = SemanticSpecs.TDB_DATASET.getDefaultModel();

	// project specific base uris
	public static final String PROJECT_BASE = "http://uib.no/info216/cabinProject/ontology/";
	public static final String PROJECT_CABIN_RES_BASE = SemanticSpecs.PROJECT_BASE + "cabin/resource/";
	public static final String PROJECT_AREA_RES_BASE = SemanticSpecs.PROJECT_BASE + "area/resource/";
	public static final String PROJECT_AREA_PROP_BASE = SemanticSpecs.PROJECT_BASE + "area/property/";
	public static final String PROJECT_WEATHER_RES_BASE = SemanticSpecs.PROJECT_BASE + "weather/resource/";
	public static final String PROJECT_WEATHER_PROP_BASE = SemanticSpecs.PROJECT_BASE + "weather/property/";
	
	
	// external base uris
	public static final String DBPEDIA_RESOURCE_BASE = "http://dbpedia.org/resource/";
	public static final String DBPEDIA_ONT_BASE = "http://dbpedia.org/ontology/";
	public static final String DBPEDIA_PROP_BASE = "http://dbpedia.org/property/";
	public static final String GEO_BASE = "http://www.w3.org/2003/01/geo/wgs84_pos#";
	public static final String SCHEMA_BASE = "http://schema.org/";

	
	// project specific resource types
	public static final Resource WEATHER_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.PROJECT_WEATHER_RES_BASE + "Weather");
	public static final Resource PLACE_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.PROJECT_AREA_RES_BASE + "Place");
	public static final Resource CABIN_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.DBPEDIA_RESOURCE_BASE + "Cabin");
	
	public static final Resource WEATHER_SUN_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.PROJECT_WEATHER_RES_BASE + "Weather_sun");
	public static final Resource WEATHER_OVERCAST_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.PROJECT_WEATHER_RES_BASE + "Weather_overcast");
	public static final Resource WEATHER_RAIN_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.PROJECT_WEATHER_RES_BASE + "Weather_rain");
	public static final Resource WEATHER_SNOW_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.PROJECT_WEATHER_RES_BASE + "Weather_snow");
	
	public static final Resource AREA_COUNTY_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.DBPEDIA_RESOURCE_BASE + "County");
	public static final Resource AREA_MUNICIPALITY_TYPE = SemanticSpecs.TDB_MODEL.createResource(SemanticSpecs.DBPEDIA_RESOURCE_BASE + "Municipality");

	
	// project specific predicates
	public static final Property HAS_WEATHER_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.PROJECT_WEATHER_PROP_BASE, "hasWeather");
	public static final Property IS_WEATHER_OF_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.PROJECT_WEATHER_PROP_BASE, "isWeatherOf");
	public static final Property HAS_WEATHER_TYPE_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.PROJECT_WEATHER_PROP_BASE, "hasWeatherType");
	
	public static final Property HAS_MUNICIPALITY_CODE = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.PROJECT_AREA_PROP_BASE, "hasMunicipalityCode");
	public static final Property IS_PLACE_TYPE_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.PROJECT_AREA_PROP_BASE, "isPlaceType");
	public static final Property HAS_FORECAST_URI_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.PROJECT_AREA_PROP_BASE, "hasForecastUri");
	
	// external predicates
	public static final Property LAT_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.GEO_BASE, "lat");
	public static final Property LONG_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.GEO_BASE, "long");
	public static final Property URL_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.SCHEMA_BASE, "URL");
	public static final Property IMAGE_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.SCHEMA_BASE, "image");
	public static final Property IS_PART_OF_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.DBPEDIA_ONT_BASE, "isPartOf");
	public static final Property COUNTY_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.DBPEDIA_PROP_BASE, "county");
	public static final Property MUNICIPALITY_PROP = SemanticSpecs.TDB_MODEL.createProperty(SemanticSpecs.DBPEDIA_PROP_BASE, "municipality");

	
	// project specific vocabulary prefixes (cwp = cabin weather project)
	public static final String PROJECT_CABIN_RES_PREFIX = "cwp-cabin-res";
	public static final String PROJECT_AREA_RES_PREFIX = "cwp-area-res";
	public static final String PROJECT_AREA_PROP_PREFIX = "cwp-area-prop";
	public static final String PROJECT_WEATHER_RES_PREFIX= "cwp-weather-res";
	public static final String PROJECT_WEATHER_PROP_PREFIX = "cwp-weather-prop";

	// external vocabulary prefixes
	public static final String DBPEDIA_RES_PREFIX = "dbpediaRes";
	public static final String DBPEDIA_OWL_PREFIX = "dbpediaOwl";
	public static final String GEO_PREFIX = "geo";
	public static final String SCHEMA_PREFIX = "schema";
	public static final String SKOS_PREFIX = "skos";
	public static final String RDF_PREFIX = "rdf";
	public static final String RDFS_PREFIX = "rdfs";
	

	/**
	 * Add external ontologies to the model
	 */
	public static void addOnt() { // messes with the model
		SharedData sharedData = SharedData.getInstance();
		
		try {
			SemanticSpecs.TDB_MODEL.read(new FileInputStream(sharedData.getSkosOntologyPath()), null);
			SemanticSpecs.TDB_MODEL.read(new FileInputStream(sharedData.getWeatherMergedOntologyPath()), null);
		} catch (FileNotFoundException e) {
			System.err.println("Ontology files cannot be found. ");
			e.printStackTrace();
		}
	}

	
	/**
	 * A OntModel with Owl DL Mem RDFS INF reasoning
	 * @return a OntModel with Owl DL Mem RDFS INF reasoning
	 */
	public static OntModel getOntModel() {
		Model tdbModel = TDB_MODEL; 
		tdbModel = getDataset().getDefaultModel();

		OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_RDFS_INF, tdbModel);
		return ontModel;
	}

	/**
	 * Opens the dataset if it has been closed
	 * @return a the dataset ddefault model
	 */
	public static Model getModel() {
		Model tdbModel = getDataset().getDefaultModel();
		return tdbModel;
	}
	
	
	/**
	 * Opens and returns the TDB dataset
	 * @return TDB dataset
	 */
	public static Dataset getDataset() {
		Dataset dataset = TDBFactory.createDataset(SharedData.getInstance().getTdbPath());
		return dataset;
	}
	

	/**
	 * Opens the TDB dataset
	 */
	public static void openDataset() {
		getDataset();
	}
	
	
	/**
	 * Closes the TDB dataset
	 */
	public static void closeDataset() {
		getDataset().close();
	}


}
package no.uib.info216.cabinwebproject.util;

import java.net.MalformedURLException;
import java.net.URL;

import no.uib.info216.cabinwebproject.ontology.Skos;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.RDF;

public class StringUtil {
	
	// html entities
	public static final String UPPERCASE_AE = "&AElig;";
	public static final String UPPERCASE_OE = "&Oslash;";
	public static final String UPPERCASE_AA = "&Aring;";
	
	public static final String LOWERCASE_AE = UPPERCASE_AE.toLowerCase();
	public static final String LOWERCASE_OE = UPPERCASE_OE.toLowerCase() ;
	public static final String LOWERCASE_AA = UPPERCASE_AA.toLowerCase();
	
	
	/**
	 * Takes a textual input and replaces special characters with HTML entity equivalent
	 * @param input The String to have special characters replaced
	 * @return a String with HTML entities in place of special characters
	 */
	public static String escapeHtml(String input) {
		String output = input.replace("Æ", UPPERCASE_AE)
			.replace("æ", LOWERCASE_AE)
			.replace("Ø", UPPERCASE_OE)
			.replace("ø", LOWERCASE_OE)
			.replace("Å", UPPERCASE_AA)
			.replace("å", LOWERCASE_AA)
			.replace("Ã˜", UPPERCASE_OE)
			.replace("Ã¸", LOWERCASE_OE)
			.replace("Ã¥", LOWERCASE_AA)
			.replace("Ã…", UPPERCASE_AA)
			.replace("Ã¦", LOWERCASE_AE);

		return output;
	}
	
	
	/**
	 * Takes a string in semantic "Literal" format and returns a String without type annotations 
	 * @param str The text string to have "literal" type data stripped
	 * @return a string without "literal" type data
	 */
	public static String escapeLiteral(String str) {
		if(str.indexOf("^^") != -1)
			str = str.substring(0, str.indexOf("^^"));
		
		return str;
	}
	
	
	/**
	 * Takes a semantic Literal and returns a String without type annotations 
	 * @param literal The semantic Literal to have type data stripped
	 * @return a string without "literal" type data
	 */
	public static String escapeLiteral(Literal literal) {
		return escapeLiteral(literal.toString());
	}
	
	
	/**
	 * Creates a token version of a string. E.g. replaces " " with "_". 
	 * @param str the token is based upon this string
	 * @param uppercaseFirstLetter flag to make the first letter upper case or not
	 * @param stripDelimiters flag to remove characters like "," and ".".
	 * @return token string with certain characters removed or replaced
	 */
	@Deprecated
	public static String strTokenizer(String str, boolean uppercaseFirstLetter, boolean stripDelimiters) {
		String token = str.replace(" ", "_").toLowerCase().trim();
		
		if(stripDelimiters) {
			token = token.replace(".", "").replace(",", "");
		}
		
		if(uppercaseFirstLetter) {
			String firstLetter = token.substring(0, 1).toUpperCase();
			token = firstLetter + token.substring(1);
		}

		return token;
	}
	
	/**
	 * Creates a token version of a string. E.g. replaces " " with "_". 
	 * @param str the token is based upon this string
	 * @param uppercaseFirstLetter flag to make the first letter upper case or not
	 * @return token string with certain characters removed or replaced
	 */
	@Deprecated
	public static String strTokenizer(String str, boolean uppercaseFirstLetter) {
		return strTokenizer(str, uppercaseFirstLetter, false);
	}
	
	
	/**
	 * Creates a token version of a string. E.g. replaces " " with "_". 
	 * @param str the token is based upon this string
	 * @return token string with certain characters removed or replaced
	 */
	@Deprecated
	public static String strTokenizer(String str) {
		return strTokenizer(str, false);
	}
	
	
	/**
	 * Creates a token based on some semantic conventions. 
	 * It replaces or removes characters that are not uri-friendly, such as " " with "_". 
	 * @param str the token is based upon this string
	 * @param uppercaseFirstLetter flag to make the first letter upper case or not
	 * @return a "semantic" token string with certain characters removed or replaced
	 *         in order to be uri-friendly
	 */
	public static String semanticTokenizer(String str, boolean uppercaseFirstLetter) {
		String token = str.replace(" ", "_").replace(".", "").replace(",", "")
				.toLowerCase().trim();
		
		if(uppercaseFirstLetter) {
			String firstLetter = token.substring(0, 1).toUpperCase();
			token = firstLetter + token.substring(1);
		}
		
		return token;
	}
	
	
	/**
	 * Wrapper for <em>MalformedURLException</em>.
	 * @param urlStr the string to be returned as a <em>URL</em> class type
	 * @return string url as a <em>URL</em>, null if it is malformed. 
	 */
	public static URL encodeUrl(String urlStr) {
		URL url = null;
		
		try {
			url = new URL(urlStr);
		} catch (MalformedURLException e) {
			System.err.println("url <" + urlStr + "> is not a proper url. ");
		}
		
		return url;
	}
	
}

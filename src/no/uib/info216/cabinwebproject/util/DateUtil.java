package no.uib.info216.cabinwebproject.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.text.DateFormatter;

import com.hp.hpl.jena.sparql.function.library.date;

public class DateUtil {

	/**
	 * Generate the current date in the ISO date format 
	 * @return the date in the ISO date format
	 */
	public static String getIsoDate() {
		return getIsoDate(new Date());
	}
	
	
	/**
	 * Generate the current date in the ISO date format 
	 * @return the date in the ISO date format
	 */
	public static String getIsoDate(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String isoDate = df.format(date);
		
		return isoDate;
	}
	
}


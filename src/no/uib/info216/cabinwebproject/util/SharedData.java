package no.uib.info216.cabinwebproject.util;

public class SharedData {

	private static SharedData instance;
	
	private String projectBasePath;
	private String tdbWrapperPath;
	private String tdbPath;
	
	private String cabinResourcePath;
	private String weatherPlaceResourcePath;
	
	private String skosOntologyPath;
	private String weatherMergedOntologyPath;
	
	private String weatherPlaceTmpPath;
	private String weatherPlaceDaoTmpPath;
	
	
	/**
	 * A shared class that holds location based data. 
	 * Singleton pattern because the data is used both in regular mode and server mode. 
	 */
	public SharedData() {
		projectBasePath = "";
		tdbWrapperPath = "tdb/";
		tdbPath = tdbWrapperPath + "cabin_weather";
		
		cabinResourcePath = "Resources/cabins.json";
		weatherPlaceResourcePath = "Resources/noreg.txt";
		
		skosOntologyPath = "Ontologies/skos.rdf";
		weatherMergedOntologyPath = "Ontologies/weather-merged.owl"; 
		
		weatherPlaceTmpPath = "Resources/cabinWeatherPlaces.data";
		weatherPlaceDaoTmpPath = "Resources/WeatherPlaceDao.data";
	}
	
	/**
	 * Singleton pattern to get instance
	 * @return <em>SharedData</em> instance
	 */
	public static SharedData getInstance() {
		if(instance == null) {
			instance = new SharedData();
		}
		return instance;
	}


	/**
	 * @return the projectBasePath
	 */
	public String getProjectBasePath() {
		return projectBasePath;
	}


	/**
	 * @param projectBasePath the projectBasePath to set
	 */
	public void setProjectBasePath(String projectBasePath) {
		this.projectBasePath = projectBasePath;
	}


	/**
	 * @return the tdbWrapperPath
	 */
	public String getTdbWrapperPath() {
		return projectBasePath + tdbWrapperPath;
	}


	/**
	 * @return the tdbPath
	 */
	public String getTdbPath() {
		return projectBasePath + tdbPath;
	}


	/**
	 * @return the cabinResourcePath
	 */
	public String getCabinResourcePath() {
		return projectBasePath + cabinResourcePath;
	}


	/**
	 * @return the weatherPlaceResourcePath
	 */
	public String getWeatherPlaceResourcePath() {
		return projectBasePath + weatherPlaceResourcePath;
	}


	/**
	 * @return the skosOntologyPath
	 */
	public String getSkosOntologyPath() {
		return projectBasePath + skosOntologyPath;
	}


	/**
	 * @return the weatherMergedOntologyPath
	 */
	public String getWeatherMergedOntologyPath() {
		return projectBasePath + weatherMergedOntologyPath;
	}


	public String getWeatherPlaceTmpPath() {
		return projectBasePath + weatherPlaceTmpPath;
	}


	/**
	 * @return the weatherPlaceDaoTmpPath
	 */
	public String getWeatherPlaceDaoTmpPath() {
		return projectBasePath + weatherPlaceDaoTmpPath;
	}
}

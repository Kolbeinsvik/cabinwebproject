package no.uib.info216.cabinwebproject.query;

import java.text.SimpleDateFormat;
import java.util.Date;

import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.ontology.WeatherOnt;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC;

public class DatasetQuery {


	public static ResultSet getWeatherPlaces(){
		Model tdbModel = SemanticSpecs.TDB_MODEL;

		Query selectQuery = QueryFactory.create(
				"SELECT ?x WHERE {"
						+ "?x <"+Skos.altLabel +"> 'weatherPlace' ."
						+"}");
		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);
		return exec.execSelect();
	}

	/**
	 * Returns a ResultSet containing weatherdata for a specific cabin on a specific date.
	 * From the resultset one can use variables:
	 * <ul>
	 * <li><b>temperature</b> (forcasted temperature for the cabin)</li>
	 * <li><b>rain</b> (forcasted precipitation for the cabin)</li>
	 * <li><b>weathertype</b> (Sun, Rain, Snow or Overcast)</li>
	 * </ul>
	 * @param label The cabin's name
	 * @param date Desired date
	 */
	public static ResultSet getWeatherDataForPlace(String label, Date date){
		//		Model tdbModel = SemanticSpecs.TDB_MODEL;
		OntModel tdbModel = SemanticSpecs.getOntModel();

		String dateString = SimpleDateFormat.getDateInstance().format(date);
		Query selectQuery = QueryFactory.create(
				"SELECT * WHERE {"
						+ "?cabinURI <" +Skos.altLabel +"> '"+label.toLowerCase()+"' ."
						+ "?cabinURI <"+WeatherOnt.hasTemperature+"> ?temperature ." 		//temperature variable
						+ "?cabinURI <"+WeatherOnt.hasPrecipitation+"> ?rain ."				//precipitation variable
						+ "?cabinURI <"+DC.date+"> '"+dateString+"' ."
						+ "?cabinURI <"+SemanticSpecs.HAS_WEATHER_TYPE_PROP+"> ?weathertype ."	//weather type
						+"}");
		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);
		return exec.execSelect();
	}

	/**
	 * Queries the TDB for a total number of the supplied resource type
	 * @return a <em>ResultSet</em> with the total count.
	 *         The number is returned without a type attribute
	 */
	public static ResultSet getCount(String varName, Resource res) {
		OntModel tdbModel = SemanticSpecs.getOntModel();
		Query selectQuery = QueryFactory.create(""
				+ "SELECT (STR(COUNT(?uri)) as ?" + varName + ") WHERE {"
				+ "?uri a <" + res + "> ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}


	public static ResultSet getCabinsInMinicipality(String muniUri){
		OntModel tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(
				"SELECT * WHERE {"
						+ "?cabinUri a <" + SemanticSpecs.CABIN_TYPE + "> ."
						+ "?cabinUri <" + SemanticSpecs.IS_PART_OF_PROP + ">" + "<" + muniUri + "> ."
						+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}



	/**
	 * Checks if there is any individual in the TDB with a specific resource type
	 * @param res The <em>Resource</em> an individual must belong to
	 * @return true if any exists, false if not
	 */
	public static boolean anyOfTypeExists(Resource res) {
		Model tdbModel = SemanticSpecs.getOntModel();

		Query askQuery = QueryFactory.create(""
				+ "ASK {"
				+ "?x a <" + res + "> ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(askQuery, tdbModel);
		boolean exists = queryExecution.execAsk();

		return exists;
	}


	/**
	 *
	 * @return
	 */

	public static ResultSet getAllOfType(Resource res) {
		Model tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(""
				+ "SELECT ?uri ?predicate ?object WHERE {"
				+ "?uri a <" + res + "> ."
				+ "?uri ?predicate ?object ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}

}

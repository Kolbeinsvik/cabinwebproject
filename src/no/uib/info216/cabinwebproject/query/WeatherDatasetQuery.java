package no.uib.info216.cabinwebproject.query;

import no.uib.info216.cabinwebproject.util.DateUtil;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.XSD;

public class WeatherDatasetQuery {


	/**
	 * 
	 * @return True/False: Weather data is updated or not
	 */
	public static boolean hasCurrentWeatherData() {
		Model tdbModel = SemanticSpecs.getOntModel();

		Query askQuery = QueryFactory.create(""
				+ "ASK {"
				+ "?weatherUri a <" + SemanticSpecs.WEATHER_TYPE + "> ."
				+ "?weatherUri <" + DC.date + "> '" + DateUtil.getIsoDate() + "'^^<" + XSD.getURI() + "string> ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(askQuery, tdbModel);
		boolean exists = queryExecution.execAsk();

		return exists;
	}


}

package no.uib.info216.cabinwebproject.query;

import java.text.SimpleDateFormat;
import java.util.Date;

import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.vocabulary.DC;

/**
 * Class containing simple SPARQL ASK-queries used in the program
 */
public class DatasetQueryTests {

	/**
	 * Checks if a named weather place exists. Important: it does not count places that are cabins.
	 * @param label Name of the cabin to look for in tdb
	 * @return Whether the place exists in tdb or not
	 */
	public static boolean weatherPlaceExists(String label){
		Model tdbmodel = SemanticSpecs.TDB_MODEL;
		assert tdbmodel != null : "No existing tdb";
		label = label.toLowerCase();

		Query askQuery = QueryFactory.create(
				"ASK {"
						+ "?x <"+Skos.altLabel +"> '" + label + "' ." +
						"?x a <" + SemanticSpecs.WEATHER_TYPE + "> ." +
						"}"
				);
		QueryExecution exec = QueryExecutionFactory.create(askQuery, tdbmodel);
		boolean result = exec.execAsk();
		return result;
	}

	/**
	 * Checks if any weather place exists. Important: it does not count places that are cabins.
	 * @return Whether any place exists in tdb or not
	 */
	public static boolean anyWeatherPlaceExists(){
		Model tdbmodel = SemanticSpecs.TDB_MODEL;
		assert tdbmodel != null : "No existing tdb";

		Query askQuery = QueryFactory.create(
				"ASK {"
						+ "?x a <" + SemanticSpecs.WEATHER_TYPE + "> ."
						+ "}"
				);
		QueryExecution exec = QueryExecutionFactory.create(askQuery, tdbmodel);
		boolean result = exec.execAsk();
		return result;
	}

	/**
	 * 
	 * @param label Name of the cabin
	 * @param date Date to check data up against
	 * @return Whether forecast-data concerning <b>label</b> on <b>date</b> exists in tdb or not
	 */
	public static boolean weatherIsUpdated(String label, Date date){
		Model tdbmodel = SemanticSpecs.TDB_MODEL;
		label = label.toLowerCase();
		String askDate = SimpleDateFormat.getDateInstance().format(date);

		Query askQuery = QueryFactory.create(
				"ASK {"
						+ "?x <"+Skos.altLabel+"> '"+label+"' ."
						+ "?x <"+DC.date+"> '"+askDate+"' ."
						+"}");
		boolean result = QueryExecutionFactory.create(askQuery, tdbmodel).execAsk();
		return result;
	}

	/**
	 * Checks whether the query starts with "ask"
	 */
	public static boolean isAskQuery(String query) {
		query = query.toLowerCase();
		query = query.trim();
		return query.startsWith("ask");
	}

	/**
	 * Checks whether the query starts with "select"
	 */
	public static boolean isSelectQuery(String query) {
		query = query.toLowerCase();
		query = query.trim();
		return query.startsWith("select");
	}

	/**
	 * Checks whether the query starts with "construct"
	 */
	public static boolean isConstructQuery(String query) {
		query = query.toLowerCase();
		query = query.trim();
		return query.startsWith("construct");
	}

}

package no.uib.info216.cabinwebproject.query;

import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.vocabulary.XSD;



public class AreaDatasetQuery {

	/**
	 * Queries the TDB for any municipalities with a given code (i.e. <em>1244</em> for <em>Austevoll</em>)
	 * @param code the supplied municipality code (e.g. <em>1201</em>, <em>1244</em> etc.)
	 * @return a <em>ResultSet</em> with the municipality name (var: <em>label</em>)
	 *         as a pure literal (no type or language attribute).
	 */
	public static ResultSet getMinicipality(int code){
		Model tdbModel = SemanticSpecs.getModel();

		Query selectQuery = QueryFactory.create(
				"SELECT (STR(?labelLiteral) as ?label) ?muniUri WHERE {"
						+ "?muniUri a <" + SemanticSpecs.AREA_MUNICIPALITY_TYPE + "> ."
						+ "?muniUri <" + SemanticSpecs.HAS_MUNICIPALITY_CODE + "> " + code + " ."
						+ "?muniUri <" +  Skos.prefLabel + "> ?labelLiteral ."
						+ "}");
		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);

		return exec.execSelect();
	}


	/**
	 * 
	 * @return Jena ResulSet with all predicates and objects attached to municipalities
	 */
	public static ResultSet getMunicipalities() {
		Model tdbModel = SemanticSpecs.getModel();

		Query selectQuery = QueryFactory.create(""
				+ "SELECT * WHERE {"
				+ "?muniUri a <" + SemanticSpecs.AREA_MUNICIPALITY_TYPE + "> ."
				+ "?muniUri ?x ?y ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}


	/**
	 * Method for getting municipality-<b>code</b> and <b>skos:prefLabel</b> for municipalities in tdb
	 * @return ResultSet from SPARQL query
	 */
	public static ResultSet getAllMunicipalityData() {
		Model tdbModel = SemanticSpecs.getModel();

		Query selectQuery = QueryFactory.create(""
				+ "SELECT (STR(?codeLiteral) as ?code) (STR(?prefLabelLiteral) as ?prefLabel) WHERE {"
				+ "?muniUri a <" + SemanticSpecs.AREA_MUNICIPALITY_TYPE + "> ."
				+ "?muniUri <" + SemanticSpecs.HAS_MUNICIPALITY_CODE + "> ?codeLiteral ."
				+ "?muniUri <" + Skos.prefLabel + ">?prefLabelLiteral ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}



	/**
	 * @return True/False whether there exists any municipality-data for municipality <b>label</b> in tdb
	 */
	public static boolean municipalityExists(String label) {
		Model tdbModel = SemanticSpecs.TDB_MODEL;

		Query selectQuery = QueryFactory.create(""
				+ "ASK {"
				+ "?x <" + Skos.altLabel + "> '" + label.toLowerCase() + "' ."
				+ "?x a <"+SemanticSpecs.AREA_MUNICIPALITY_TYPE+"> ."
				+ "}"
				+ "LIMIT 1");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		boolean exists = queryExecution.execAsk();

		return exists;
	}


	/**
	 * @return True/False whether there exists any municipality-data in tdb
	 */
	public static boolean anyMunicipalityExists() {
		Model tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(""
				+ "ASK {"
				+ "?x a <" + SemanticSpecs.AREA_MUNICIPALITY_TYPE + "> ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		boolean exists = queryExecution.execAsk();

		return exists;
	}

	/**
	 * @return Jena ResultSet with all forecast-uri's in tdb
	 */
	public static ResultSet getAllForecastUris() {
		Model tdbModel = SemanticSpecs.getModel();

		Query selectQuery = QueryFactory.create(""
				+ "SELECT (STR(?forecastUriLiteral) as ?forecastUri) WHERE {"
				+ "?placeUri a <" + SemanticSpecs.PLACE_TYPE + "> ."
				+ "?placeUri <" + SemanticSpecs.HAS_FORECAST_URI_PROP + "> ?forecastUriLiteral ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}




	public static ResultSet getAllHutPlaces() {
		Model tdbModel = SemanticSpecs.getModel();

		Query selectQuery = QueryFactory.create(""
				+ "SELECT DISTINCT (STR(?placeNameLiteral) as ?placeName) (STR(?placeTypeLiteral) as ?placeType) "
				+ "?municipalityRes (STR(?latitudeLiteral) as ?latitude) (STR(?longitudeLiteral) as ?longitude) "
				+ "(STR(?forecastUriLiteral) as ?forecastUrl)"
				+ " WHERE {"
				+ "?placeUri a <" + SemanticSpecs.PLACE_TYPE + "> ."
				+ "?placeUri <" + Skos.prefLabel + "> ?placeNameLiteral ."
				+ "?placeUri <" + SemanticSpecs.IS_PLACE_TYPE_PROP + "> ?placeTypeLiteral ."
				+ "?placeUri <" + SemanticSpecs.IS_PLACE_TYPE_PROP + "> 'Hut'^^<" + XSD.getURI() + "string> ."
				+ "?placeUri <" + SemanticSpecs.MUNICIPALITY_PROP + "> ?municipalityRes ."
				+ "?placeUri <" + SemanticSpecs.LAT_PROP + "> ?latitudeLiteral ."
				+ "?placeUri <" + SemanticSpecs.LONG_PROP + "> ?longitudeLiteral ."
				+ "?placeUri <" + SemanticSpecs.HAS_FORECAST_URI_PROP + "> ?forecastUriLiteral ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		ResultSet results = queryExecution.execSelect();

		return results;
	}

}

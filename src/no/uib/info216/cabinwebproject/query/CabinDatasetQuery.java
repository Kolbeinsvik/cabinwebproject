package no.uib.info216.cabinwebproject.query;

import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.ontology.WeatherOnt;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC;

public class CabinDatasetQuery {




	/**
	 * Returns a <em>ResultSet</em>  with all data related to a named cabin.
	 * Note: data must be connected in the TDB to be returned.
	 * @param label Name of the cabin
	 * @return ResultSet with all cabin data
	 */
	public static ResultSet getCabinData(String label){
		Model tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(
				"SELECT * WHERE {"
						+"?cabinUri <" + Skos.altLabel + "> '"+label.toLowerCase()+"' ."
						+"?cabinUri a <" + SemanticSpecs.CABIN_TYPE + "> ."
						+"?cabinUri ?x ?y ."
						+"}");
		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);
		return exec.execSelect();
	}



	/**
	 * Returns a <em>ResultSet</em>  with all data related to a named cabin.
	 * Note: data must be connected in the TDB to be returned.
	 * @param label Name of the cabin
	 * @return ResultSet with all cabin data
	 */
	public static ResultSet getCabinData(String cabinUri, Resource weatherTypeRes){
		OntModel tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(
				"SELECT ?prefLabel (STR(?utUrlLiteral) as ?utUrl) "
						+ "(STR(?rainLiteral) as ?rain) (STR(?temperatureLiteral) as ?temperature) "
						+ "(STR(?municipalityLiteral) as ?municipalityLabel) (STR(?countyLiteral) as ?countyLabel) "
						+ "(STR(?dateLiteral) as ?isoDate) "
						+ "WHERE {"
						+"OPTIONAL{"
						+ "<" + cabinUri + "> <" + Skos.prefLabel + "> ?prefLabel ."
						+ "<" + cabinUri + "> <" + SemanticSpecs.URL_PROP + "> ?utUrlLiteral ."
						+ "<" + cabinUri + "> <" + SemanticSpecs.HAS_WEATHER_PROP + "> ?weatherUri ."

						+ "<" + cabinUri + "> <" + SemanticSpecs.MUNICIPALITY_PROP + "> ?muniUri ."
						+ "?muniUri a <" + SemanticSpecs.AREA_MUNICIPALITY_TYPE + "> ."
						+ "?muniUri <" + Skos.prefLabel + "> ?municipalityLiteral ."

						+ "?muniUri <" + SemanticSpecs.COUNTY_PROP + "> ?countyUri ."
						+ "?countyUri a <" + SemanticSpecs.AREA_COUNTY_TYPE + "> ."
						+ "?countyUri <" + Skos.prefLabel + "> ?countyLiteral ."

						+ "?weatherUri a <" + SemanticSpecs.WEATHER_TYPE + "> ."
						+ "?weatherUri a <" + weatherTypeRes + "> ."
						+ "?weatherUri <" + WeatherOnt.hasTemperature + "> ?temperatureLiteral ." 	//temperature variable
						+ "?weatherUri <" + WeatherOnt.hasPrecipitation + "> ?rainLiteral ."		//precipitation variable
						+ "?weatherUri <" + DC.date + "> ?dateLiteral ."									//date
						+"}"
						+"}");
		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);
		return exec.execSelect();
	}


	/**
	 * 
	 * @param label Name of the cabin to search for
	 * @return Whether any data exist in tdb for this cabin
	 */
	public static boolean cabinExists(String label) {
		Model tdbModel = SemanticSpecs.TDB_MODEL;
		label = label.toLowerCase();

		Query selectQuery = QueryFactory.create(""
				+ "ASK {"
				+ "?cabinURI <" + Skos.altLabel + "> '" + label + "' ."
				+ "?cabinURI a <"+SemanticSpecs.CABIN_TYPE+"> ."
				+ "}");

		QueryExecution queryExecution = QueryExecutionFactory.create(selectQuery, tdbModel);
		boolean exists = queryExecution.execAsk();

		return exists;
	}

	/**
	 * @param muniUri URI for municipality
	 * @return Jena ResulSet containing all cabinURI's++ located in the municipality
	 */
	public static ResultSet getCabinsInMinicipality(String muniUri){
		OntModel tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(
				"SELECT * WHERE {"
						+ "?cabinUri a <" + SemanticSpecs.CABIN_TYPE + "> ."
						+ "?cabinUri <" + SemanticSpecs.MUNICIPALITY_PROP + ">" + "<" + muniUri + "> ."
						+ "}");

		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);
		return exec.execSelect();
	}


	/**
	 * @param muniCode Integer code for municipality
	 * @return Jena ResulSet containing all cabinURI's++ located in the municipality
	 */
	public static ResultSet getCabinsInMinicipality(int muniCode){
		OntModel tdbModel = SemanticSpecs.getOntModel();

		Query selectQuery = QueryFactory.create(
				"SELECT (STR(?labelLiteral) as ?label) ?cabinUri WHERE {"
						+ "?cabinUri a <" + SemanticSpecs.CABIN_TYPE + "> ."
						+ "?muniUri a <" + SemanticSpecs.AREA_MUNICIPALITY_TYPE + "> ."
						+ "?muniUri <" + Skos.altLabel + "> " + muniCode + " ."
						+ "?cabinUri <"+SemanticSpecs.IS_PART_OF_PROP+"> ?muniUri ."
						+ "?cabinUri <" + Skos.prefLabel + "> ?labelLiteral ."
						+ "}"
						+ "");

		QueryExecution exec = QueryExecutionFactory.create(selectQuery, tdbModel);
		return exec.execSelect();
	}
}

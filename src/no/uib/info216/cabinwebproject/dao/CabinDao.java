package no.uib.info216.cabinwebproject.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import no.uib.info216.cabinwebproject.model.WeatherModel;
import no.uib.info216.cabinwebproject.model.CabinModel;
import no.uib.info216.cabinwebproject.model.MunicipalityModel;
import no.uib.info216.cabinwebproject.model.SemanticCabinModel;
import no.uib.info216.cabinwebproject.model.WeatherTypeModel;
import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.ontology.WeatherOnt;
import no.uib.info216.cabinwebproject.query.CabinDatasetQuery;
import no.uib.info216.cabinwebproject.query.DatasetQuery;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;

public class CabinDao {

	private static CabinDao instance = null;

	private List<CabinModel> cabins;

	public CabinDao() {
		this.cabins = new ArrayList<CabinModel>();
	}


	public static CabinDao getInstance() {
		if(instance == null) {
			instance = new CabinDao();
		}

		return instance;
	}


	public void add(CabinModel cabin) {
		this.cabins.add(cabin);
	}


	public void addAll(List<CabinModel> cabins) {
		this.cabins = cabins;
	}
	
	
	
	/**
	 * Does not work. Will always return <strong>null</strong>
	 * @param label
	 * @return
	 */
	public static SemanticCabinModel getCabin(String label) {
		SemanticCabinModel cabin = null;
		
		ResultSet cabinResults = CabinDatasetQuery.getCabinData(label);
		while(cabinResults.hasNext()) {
			QuerySolution cabinSolution = cabinResults.next();
			String subject = cabinSolution.get("cabinUri").toString();
			String predicate = cabinSolution.get("x").toString();
			String object = cabinSolution.get("y").toString();
			
			System.out.println("[" + subject + "] " + predicate + " -> " + object);
		}
		
		SemanticSpecs.closeDataset();
		
		return cabin;
	}
	
	
	
	/**
	 * Does not work. Will always return <strong>null</strong>
	 * @param label
	 * @return
	 */
	public static SemanticCabinModel getCabin(String cabinUri, Resource weatherTypeRes) {
		SemanticCabinModel cabin = null;
		
		ResultSet cabinResults = CabinDatasetQuery.getCabinData(cabinUri, weatherTypeRes);
		while(cabinResults.hasNext()) {
			QuerySolution solution = cabinResults.next();

			if(!solution.varNames().hasNext()) continue;
			
			String label = solution.getLiteral("prefLabel").toString();
			String utUrl = solution.getLiteral("utUrl").toString();

			String county = solution.getLiteral("countyLabel").toString();
			String municipality = solution.getLiteral("municipalityLabel").toString();

			String rainStr = solution.getLiteral("rain").toString();
			String temperatureStr = solution.getLiteral("temperature").toString();
			String isoDateStr = solution.getLiteral("isoDate").toString();

			float rain = Float.parseFloat(rainStr);
			float temperature = Float.parseFloat(temperatureStr);

			WeatherModel weather = new WeatherModel(null, isoDateStr, rain, temperature);
			cabin = new SemanticCabinModel(label, utUrl, county, municipality, weather);
		}
		
		SemanticSpecs.closeDataset();
		
		return cabin;
	}


	/**
	 * 
	 */
	public static List<SemanticCabinModel> getCabins(WeatherTypeModel weatherType, List<MunicipalityModel> municipalities) {
		List<SemanticCabinModel> semanticCabins = new ArrayList<SemanticCabinModel>();
		
		Resource weatherTypeRes = WeatherTypeModel.getResource(weatherType);
		
		for(MunicipalityModel municipality : municipalities) {
			ResultSet cabinResults = CabinDatasetQuery.getCabinsInMinicipality(municipality.getSemanticUri());
			
			//			List<Resource> cabinResList = buildCabinResList(cabinResults);
			//			for(Resource cabinRes : cabinResList) {
			//				SemanticCabinModel semanticCabin = buildSemanticCabinProgramatically(cabinRes, weatherType);
			//				semanticCabins.add(semanticCabin);
			//			}

			List<String> cabinUriList = buildCabinUriList(cabinResults);
			for(String cabinUri : cabinUriList) {
//				SemanticCabinModel semanticCabin = buildSemanticCabinSemantically(cabinUri, weatherType);
				SemanticCabinModel semanticCabin = buildSemanticCabin(cabinUri, weatherTypeRes);
				
				if(semanticCabin != null && !listContains(semanticCabins, semanticCabin))
					semanticCabins.add(semanticCabin);
			}
			
			SemanticSpecs.closeDataset();
		}


		return semanticCabins;
	}

public static boolean listContains(List<SemanticCabinModel> list, SemanticCabinModel cabin) {
		for (SemanticCabinModel foundCabin : list) {
			if (foundCabin.getLabel().equals(cabin.getLabel())) {
				return true;
			}
		}
		return false;
	}
	
	public static List<String> buildCabinUriList(ResultSet cabinResults) {
		List<String> cabinUriList = new ArrayList<String>();

		while(cabinResults.hasNext()) {
			QuerySolution solution = cabinResults.nextSolution();
			String cabinUri = solution.getResource("cabinUri").toString();

			if(cabinUri != null) {
				cabinUriList.add(cabinUri);
			}
		}

		return cabinUriList;
	}


	public static List<Resource> buildCabinResList(ResultSet cabinResults) {
		List<Resource> cabinResList = new ArrayList<Resource>();

		while(cabinResults.hasNext()) {
			QuerySolution solution = cabinResults.nextSolution();
			Resource cabinRes = solution.getResource("cabinUri");

			if(cabinRes != null) {
				cabinResList.add(cabinRes);
			}
		}

		return cabinResList;
	}
	
	
	public static SemanticCabinModel buildSemanticCabin(String cabinUri, Resource weatherType) {
		SemanticCabinModel cabin = CabinDao.getCabin(cabinUri, weatherType);
		
		return cabin;
	}


	public static SemanticCabinModel buildSemanticCabinProgramatically(Resource cabinRes, WeatherTypeModel weatherType) {
		SemanticCabinModel semanticCabin = null;


		//		StmtIterator cabinIter = cabinRes.listProperties();
		//		while(cabinIter.hasNext()) {
		//			System.out.println(cabinIter.nextStatement());
		//		}

		Resource weatherTypeRes = cabinRes.getProperty(SemanticSpecs.HAS_WEATHER_TYPE_PROP).getResource();
		System.out.println(weatherTypeRes);

		//		StmtIterator typeIter = weatherTypeRes.listProperties();
		//		while(typeIter.hasNext()) {
		//			System.out.println(typeIter.nextStatement());
		//		}

		Resource areaRes = cabinRes.getProperty(SemanticSpecs.IS_PART_OF_PROP).getResource();
		//		StmtIterator areaIter = areaRes.listProperties();
		//		while(areaIter.hasNext()) {
		//			System.out.println(areaIter.nextStatement());
		//		}

		String label = cabinRes.getProperty(Skos.prefLabel).getString();
		String utUrl = cabinRes.getProperty(SemanticSpecs.URL_PROP).getString();
		//		String weatherType = "weatherType";
		float rain = cabinRes.getProperty(WeatherOnt.hasPrecipitation).getFloat();
		float temperature = cabinRes.getProperty(WeatherOnt.hasTemperature).getFloat();

		String county = areaRes.getProperty(Skos.prefLabel).getString();
		String municipality = areaRes.getProperty(Skos.prefLabel).getString();


//		semanticCabin = new SemanticCabinModel(label, utUrl, weatherType, rain, temperature, county, municipality);
		return semanticCabin;
	}


//	public static SemanticCabinModel buildSemanticCabinSemantically(String cabinUri, WeatherTypeModel weatherType) {
//		SemanticCabinModel semanticCabin = null;
//
//		ResultSet cabinResults = DatasetQuery.getWeatherDataFromUri(cabinUri, new Date(), weatherType.getTypeUri());
//		
//		while(cabinResults.hasNext()) {
//			QuerySolution solution = cabinResults.nextSolution();
//			
//			// skip this if the cabin exists but we have no weather information about it
//			if(!solution.varNames().hasNext()) {
//				continue;
//			}
//
//			String label = solution.getLiteral("prefLabel").toString();
//			String utUrl = solution.getLiteral("utUrl").toString();
//
//			String rainStr = solution.getLiteral("rain").toString();
//			String temperatureStr = solution.getLiteral("temperature").toString();
//
//			String county = solution.getLiteral("countyLabel").toString();
//			String municipality = solution.getLiteral("municipalityLabel").toString();
//
//			float rain = Float.parseFloat(rainStr);
//			float temperature = Float.parseFloat(temperatureStr);
//
////			semanticCabin = new SemanticCabinModel(label, utUrl, weatherType, rain, temperature, county, municipality);
//		}
//
//		SemanticSpecs.closeDataset();
//		return semanticCabin;
//	}



	public List<CabinModel> getAll() {
		return this.cabins;
	}


	public Iterator<CabinModel> iterator() {
		return this.cabins.iterator();
	}




	public String toString() {
		String str = "CabinDao: " + this.cabins.size() + " cabins. ";

		return str;
	}

}

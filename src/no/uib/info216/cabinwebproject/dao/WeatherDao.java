package no.uib.info216.cabinwebproject.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.uib.info216.cabinwebproject.model.WeatherModel;

public class WeatherDao {

	private static WeatherDao instance = null;

	
	private List<WeatherModel> weatherData;

	
	public WeatherDao() {
		this.weatherData = new ArrayList<WeatherModel>();
	}

	
	
	public void add(WeatherModel weather) {
		weatherData.add(weather);
	}
	
	
	public Iterator<WeatherModel> iterator() {
		return weatherData.iterator();
	}

	public static WeatherDao getInstance() {
		if(instance == null) {
			instance = new WeatherDao();
		}

		return instance;
	}
}

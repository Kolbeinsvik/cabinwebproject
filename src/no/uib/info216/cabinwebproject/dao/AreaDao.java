package no.uib.info216.cabinwebproject.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.uib.info216.cabinwebproject.controller.MainController;
import no.uib.info216.cabinwebproject.model.PlaceModel;
import no.uib.info216.cabinwebproject.model.CountyModel;
import no.uib.info216.cabinwebproject.model.GeoPointModel;
import no.uib.info216.cabinwebproject.model.MunicipalityModel;
import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.query.AreaDatasetQuery;
import no.uib.info216.cabinwebproject.query.DatasetQuery;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.SemanticUtil;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;

public class AreaDao {
	
	public static final String TYPE_HUT = "Hut";
	
	private static AreaDao instance;

	private List<CountyModel> counties;
	private List<MunicipalityModel> municipalities;
	private List<PlaceModel> places;
	
	
	/**
	 * 
	 */
	public AreaDao() {
		counties = new ArrayList<CountyModel>();
		municipalities = new ArrayList<MunicipalityModel>();
		places = new ArrayList<PlaceModel>();
	}
	
	
	
	/**
	 * 
	 * @param county
	 * @return
	 */
	public boolean addCounty(CountyModel county) {
		if(containsCounty(county)) {
			return false;
		}
		
		counties.add(county);
		return true;
	}
	
	
	/**
	 * 
	 * @param municipality
	 * @param belongsToCounty
	 * @return
	 */
	public boolean addMunicipality(MunicipalityModel municipality, CountyModel belongsToCounty) {
		if(containsMunicipality(municipality)) {
			return false;
		}
		
		municipalities.add(municipality);
		getCounty(belongsToCounty).add(municipality);
		
		return true;
	}
	
	
	/**
	 * 
	 * @param place
	 * @return
	 */
	public boolean addPlace(PlaceModel place) {
		if(places.contains(place)) {
			return false;
		}
		
		places.add(place);
		return true;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Iterator<CountyModel> countyIterator() {
		return counties.iterator();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Iterator<PlaceModel> placeIterator() {
		return places.iterator();
	}
	
	
	public boolean containsCounty(CountyModel county) {
		boolean found = (getCounty(county) != null) ? true: false;
		return found;
	}
	
	
	public boolean containsMunicipality(MunicipalityModel municipality) {
		Iterator<MunicipalityModel> iter = municipalities.iterator();
		boolean found = false;
		
		while(!found && iter.hasNext()) {
			MunicipalityModel listedMunicipality = iter.next();
			
			if(listedMunicipality.getLabel().equalsIgnoreCase(municipality.getLabel())) {
				found = true;
			}
		}
		
		return found;
	}
	
	
	public CountyModel getCounty(CountyModel county) {
		Iterator<CountyModel> iter = counties.iterator();
		boolean found = false;
		CountyModel foundCounty = null;
		
		while(!found && iter.hasNext()) {
			CountyModel listedCounty = iter.next();
			
			if(listedCounty.getLabel().equalsIgnoreCase(county.getLabel())) {
				found = true;
				foundCounty = listedCounty;
			}
		}
		
		return foundCounty;
	}
	
	public List<CountyModel> getCounties() {
		return counties;
	}
	
//	public List<MunicipalityModel> getMunicipalities() {
//		return municipalities;
//	}
	
	/**
	 * Creates a list of the municipalities that has the supplied municipality codes
	 * and exists in the tdb.   
	 * @param muniCodes
	 * @return a <em>List&lt;MunicipalityModel&gt;</em> of the municipalities
	 */
	public static List<MunicipalityModel> getMunicipalities(Integer[] muniCodes) {
		List<MunicipalityModel> muniList = new ArrayList<MunicipalityModel>();
		
		for(int code : muniCodes) {
			ResultSet results = AreaDatasetQuery.getMinicipality(code);

			if(results.hasNext()) {
				QuerySolution solution = results.nextSolution();
				String muniStr = solution.getLiteral("label").toString();
				String semanticUri = solution.getResource("muniUri").getURI();

				MunicipalityModel muni = new MunicipalityModel(code, muniStr);
				muni.setSemanticUri(semanticUri);
				muniList.add(muni);
			}
		}
		
		SemanticSpecs.closeDataset();
		return muniList;
	}
	
	
	public static List<MunicipalityModel> getMunicipalities() {
		List<MunicipalityModel> municipalities = new ArrayList<MunicipalityModel>();
		ResultSet results = AreaDatasetQuery.getAllMunicipalityData();
		
		while(results.hasNext()) {
			QuerySolution solution = results.next();
			
			int code = SemanticUtil.parseLiteralAsInteger("code", solution);
			String prefLabel = SemanticUtil.parseLiteralAsString("prefLabel", solution);
			
			MunicipalityModel muni = new MunicipalityModel(code, prefLabel);
			municipalities.add(muni);
		}
		
		Integer[] muniCodes = new Integer[municipalities.size()];
		for(int i = 0; i < municipalities.size(); i++) {
			MunicipalityModel muni = municipalities.get(i);
			muniCodes[i] = muni.getCode();
		}
		
		return getMunicipalities(muniCodes);
	}
	
	
	public int numberOfMunicipalities() {
		return municipalities.size();
	}
	
	
	
//	public int getCountyNum() {
//		return counties.size();
//	}
	
	
//	public int getMunicipalityNum() {
//		return municipalities.size();
//	}
	
	
	public static List<String> getAllForecastUris() {
		List<String> uris = new ArrayList<String>();
		
		ResultSet forecastResults = AreaDatasetQuery.getAllForecastUris();
		while(forecastResults.hasNext()) {
			QuerySolution solution = forecastResults.next();
			String uri = SemanticUtil.parseLiteralAsString("forecastUri", solution);
			
			if(uri != null) uris.add(uri);
		}
		
		return uris;
	}
	
	
	public static List<PlaceModel> getPlaces() {
		List<PlaceModel> places = new ArrayList<PlaceModel>();
		
		ResultSet results = AreaDatasetQuery.getAllHutPlaces();
		while(results.hasNext()) {
			QuerySolution solution = results.next();
			
			String placeName = SemanticUtil.parseLiteralAsString("placeName", solution);
			String placeType = SemanticUtil.parseLiteralAsString("placeType", solution);
			Resource municipalityRes = solution.getResource("municipalityRes");
			float latitude = SemanticUtil.parseLiteralToFloat("latitude", solution);
			float longitude = SemanticUtil.parseLiteralToFloat("longitude", solution);
			String forecastUrl = SemanticUtil.parseLiteralAsString("forecastUrl", solution);
			
			
			String municipalityStr = municipalityRes.getProperty(Skos.prefLabel).getString();
			MunicipalityModel municipality = new MunicipalityModel(municipalityStr);
			GeoPointModel geoPoint = new GeoPointModel(latitude, longitude);
			
			PlaceModel place = new PlaceModel(placeName, placeType, municipality, geoPoint, forecastUrl);
			
			// only adds it if it isn't there already
			if(!placesContains(places, place))
				places.add(place);
		}
		
		return places;
	}
	
	
	public static boolean placesContains(List<PlaceModel> places, PlaceModel otherPlace) {
		boolean contains = false;
		
		Iterator<PlaceModel> iter = places.iterator();
		while(!contains && iter.hasNext()) {
			PlaceModel place = iter.next();
			
			if(place.getPlaceName().equalsIgnoreCase(otherPlace.getPlaceName()) && 
					place.getPlaceType().equalsIgnoreCase(otherPlace.getPlaceType())) {
				contains = true;
			}
		}
		
		return contains;
	}
	
	
	public static AreaDao getInstance() {
		if(instance == null) {
			instance = new AreaDao();
		}
		
		return instance;
	}
	
}
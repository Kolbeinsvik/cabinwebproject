package no.uib.info216.cabinwebproject.lifter;

import java.util.Iterator;

import no.uib.info216.cabinwebproject.dao.CabinDao;
import no.uib.info216.cabinwebproject.model.CabinModel;
import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.OWL;

public class CabinLifter {

	public static void liftCabins() {
		Model tdbModel = SemanticSpecs.TDB_MODEL;

		CabinDao cabinDao = CabinDao.getInstance();
		Iterator<CabinModel> cabinIter = cabinDao.iterator();

		while(cabinIter.hasNext()) {
			CabinModel cabin = cabinIter.next();
			Resource cabinInd = tdbModel.createResource(cabin.getSemanticUri(), SemanticSpecs.CABIN_TYPE);
			
			cabinInd.addProperty(Skos.prefLabel, cabin.getLabel());
			cabinInd.addProperty(Skos.altLabel, cabin.getLabel().toLowerCase());

			cabinInd.addLiteral(SemanticSpecs.LAT_PROP, cabin.getGeoPoint().getLatitude());
			cabinInd.addLiteral(SemanticSpecs.LONG_PROP, cabin.getGeoPoint().getLongitude());
			cabinInd.addProperty(SemanticSpecs.URL_PROP, cabin.getUtUrl());
			cabinInd.addProperty(SemanticSpecs.IMAGE_PROP, cabin.getImagePath());
			
			// checks if there is a equivalent place
			String placeUri = SemanticSpecs.PROJECT_AREA_RES_BASE + StringUtil.semanticTokenizer(cabin.getLabel(), true);
			Resource placeRes = tdbModel.getResource(placeUri);
			if(placeRes.asResource().listProperties().hasNext()) {
				cabinInd.addProperty(OWL.sameAs, placeRes);
				placeRes.addProperty(OWL.sameAs, cabinInd);
				
				Statement weatherStatement = placeRes.getProperty(SemanticSpecs.HAS_WEATHER_PROP);
				if(weatherStatement != null) {
					Resource weatherRes = weatherStatement.getResource();
					if(weatherRes.listProperties().hasNext()) {
						cabinInd.addProperty(SemanticSpecs.HAS_WEATHER_PROP, weatherRes);
					}
				}
				
				Statement muniStatement = placeRes.getProperty(SemanticSpecs.MUNICIPALITY_PROP);
				if(muniStatement != null) {
					Resource muniRes = muniStatement.getResource();
					if(muniRes.listProperties().hasNext()) {
						cabinInd.addProperty(SemanticSpecs.MUNICIPALITY_PROP, muniRes);
					}
				}
			}
		}
	}
}

package no.uib.info216.cabinwebproject.lifter;

import java.util.Iterator;

import no.uib.info216.cabinwebproject.dao.WeatherDao;
import no.uib.info216.cabinwebproject.model.WeatherModel;
import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.ontology.WeatherOnt;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;
import no.uib.info216.cabinwebproject.util.StringUtil;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.RDF;

public class WeatherLifter {


	public static void liftWeatherData() {
		Model tdbModel = SemanticSpecs.TDB_MODEL;
		Iterator<WeatherModel> iter = WeatherDao.getInstance().iterator();

		while(iter.hasNext()) {
			WeatherModel weather = iter.next();

			Resource weatherRes = tdbModel.createResource(weather.getSemanticUri(), SemanticSpecs.WEATHER_TYPE);

			String placeName = weather.getPlace().getPlaceName();

			weatherRes.addLiteral(Skos.prefLabel, placeName);
			weatherRes.addLiteral(DC.date, weather.getIsoDate());
			weatherRes.addLiteral(WeatherOnt.hasPrecipitation, weather.getPrecipitation());
			weatherRes.addLiteral(WeatherOnt.hasTemperature, weather.getTemperature());


			String placeUri = SemanticSpecs.PROJECT_AREA_RES_BASE + StringUtil.semanticTokenizer(placeName, true);
			Resource placeRes = tdbModel.getResource(placeUri).asResource();
			if(placeRes.listProperties().hasNext()) {
				placeRes.addProperty(SemanticSpecs.HAS_WEATHER_PROP, weatherRes);
			}


			// add weather type
			switch(weather.findWeatherType()){
			case "Sun" :
				weatherRes.addProperty(SemanticSpecs.HAS_WEATHER_TYPE_PROP, SemanticSpecs.WEATHER_SUN_TYPE);
				weatherRes.addProperty(RDF.type, SemanticSpecs.WEATHER_SUN_TYPE);
				break;
			case "Rain" :
				weatherRes.addProperty(SemanticSpecs.HAS_WEATHER_TYPE_PROP, SemanticSpecs.WEATHER_RAIN_TYPE);
				weatherRes.addProperty(RDF.type, SemanticSpecs.WEATHER_RAIN_TYPE);
				break;
			case "Snow" :
				weatherRes.addProperty(SemanticSpecs.HAS_WEATHER_TYPE_PROP, SemanticSpecs.WEATHER_SNOW_TYPE);
				weatherRes.addProperty(RDF.type, SemanticSpecs.WEATHER_SNOW_TYPE);
				break;
			default :
				weatherRes.addProperty(SemanticSpecs.HAS_WEATHER_TYPE_PROP, SemanticSpecs.WEATHER_OVERCAST_TYPE);
				weatherRes.addProperty(RDF.type, SemanticSpecs.WEATHER_OVERCAST_TYPE);
				break;
			}
		}
	}



}

package no.uib.info216.cabinwebproject.lifter;

import java.util.Iterator;

import no.uib.info216.cabinwebproject.dao.AreaDao;
import no.uib.info216.cabinwebproject.model.PlaceModel;
import no.uib.info216.cabinwebproject.model.CountyModel;
import no.uib.info216.cabinwebproject.model.MunicipalityModel;
import no.uib.info216.cabinwebproject.ontology.Skos;
import no.uib.info216.cabinwebproject.util.SemanticSpecs;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;


public class AreaLifter {

	private static Model tdbModel = SemanticSpecs.TDB_MODEL;
	private static AreaDao areaDao = AreaDao.getInstance();
	
	
	/**
	 * 
	 */
	public static void liftAreas() {
		Iterator<CountyModel> areaIter = areaDao.countyIterator();
		while(areaIter.hasNext()) {
			CountyModel county = areaIter.next();

			Resource countyRes = tdbModel.createResource(county.getSemanticUri(), SemanticSpecs.AREA_COUNTY_TYPE);

			countyRes.addLiteral(Skos.prefLabel, county.getLabel());
			countyRes.addLiteral(Skos.altLabel, county.getTokenLabel());


			Iterator<MunicipalityModel> municipalityIter = county.iterator();
			while(municipalityIter.hasNext()) {
				MunicipalityModel municipality = municipalityIter.next();

				Resource municipalityRes = tdbModel.createResource(municipality.getSemanticUri(), SemanticSpecs.AREA_MUNICIPALITY_TYPE);

				municipalityRes.addLiteral(Skos.prefLabel, ResourceFactory.createTypedLiteral(municipality.getLabel()));
				municipalityRes.addLiteral(Skos.altLabel, ResourceFactory.createTypedLiteral(municipality.getTokenLabel()));
				municipalityRes.addLiteral(Skos.altLabel, municipality.getCode());
				municipalityRes.addLiteral(SemanticSpecs.HAS_MUNICIPALITY_CODE, municipality.getCode());

				municipalityRes.addProperty(SemanticSpecs.IS_PART_OF_PROP, countyRes);
				municipalityRes.addProperty(SemanticSpecs.COUNTY_PROP, countyRes);
			}
		}
	}
	
	
	/**
	 * 
	 */
	public static void liftPlaceData() {
		Iterator<PlaceModel> placeIter = areaDao.placeIterator();
		while(placeIter.hasNext()) {
			PlaceModel place = placeIter.next();
			
			Resource muniRes = tdbModel.getResource(place.getMunicipality().getSemanticUri());
			
			Resource placeRes = tdbModel.createResource(place.getSemanticUri(), SemanticSpecs.PLACE_TYPE);
			
			placeRes.addLiteral(Skos.prefLabel, place.getPlaceName());
			placeRes.addLiteral(Skos.altLabel, place.getPlaceName().toLowerCase());
			
			placeRes.addLiteral(SemanticSpecs.IS_PLACE_TYPE_PROP, place.getPlaceType());

			placeRes.addLiteral(SemanticSpecs.LAT_PROP, place.getGeoPoint().getLatitude());
			placeRes.addLiteral(SemanticSpecs.LONG_PROP, place.getGeoPoint().getLongitude());
			
			placeRes.addLiteral(SemanticSpecs.HAS_FORECAST_URI_PROP, place.getForecastUrl());
			
			if(muniRes.listProperties().hasNext()) {
				placeRes.addProperty(SemanticSpecs.IS_PART_OF_PROP, muniRes);
				placeRes.addProperty(SemanticSpecs.MUNICIPALITY_PROP, muniRes);
			}
		}
	}

}



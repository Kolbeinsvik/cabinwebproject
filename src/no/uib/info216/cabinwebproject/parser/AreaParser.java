package no.uib.info216.cabinwebproject.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import no.uib.info216.cabinwebproject.dao.AreaDao;
import no.uib.info216.cabinwebproject.model.PlaceModel;
import no.uib.info216.cabinwebproject.model.CountyModel;
import no.uib.info216.cabinwebproject.model.GeoPointModel;
import no.uib.info216.cabinwebproject.model.MunicipalityModel;
import no.uib.info216.cabinwebproject.util.SharedData;

public class AreaParser {

	private static File defaultSourceFile = new File(SharedData.getInstance().getWeatherPlaceResourcePath());


	public static void parseAreas(boolean useSamplePlaces) {
		AreaDao areaDao = AreaDao.getInstance();

		Scanner sc = AreaParser.createScanner(defaultSourceFile);
		if(sc.hasNextLine()) sc.nextLine(); // skips first line headings

		while(sc.hasNextLine()) {
			String line = sc.nextLine();
//			String[] values = line.split("\t");
			String[] values = line.split("	");

			String countyLabel = values[7];
			String municipalityLabel = values[6];
			int municipalityCode = Integer.parseInt(values[0]);

			String placeName = values[1];
			String placeType = values[5];
			
			double latitude = Double.parseDouble(values[8]);
			double longitude = Double.parseDouble(values[9]);
			GeoPointModel geoPoint = new GeoPointModel(latitude, longitude);
			
			String forecastUrl = (values.length == 14) ? values[13] : null;
			

			CountyModel county = new CountyModel(countyLabel);
			MunicipalityModel municipality = new MunicipalityModel(municipalityCode, municipalityLabel);
			PlaceModel place = new PlaceModel(placeName, placeType, municipality, geoPoint, forecastUrl);
			
			
			areaDao.addCounty(county);
			areaDao.addMunicipality(municipality, county);
			
			
			if(useSamplePlaces && !samplePlacesContains(place)) continue;
			areaDao.addPlace(place);
		}

	}

	/**
	 * 
	 */
	public static void parseAreas() {
		parseAreas(false);
	}


	public static Scanner createScanner(File file) {
		Scanner sc = null;

		try {
			sc = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.err.println("Resursfilen eksisterer ikke [" + file.getName() + "]");
			e.printStackTrace();
		}

		return sc;
	}
	
	
	public static List<String> buildSamplePlaceNames() {
		List<String> places = new ArrayList<String>();
		
		places.add("Ertehytta");
		places.add("Sæter");
		places.add("Gressvasshytta");
		places.add("Senjabu");
		places.add("Lyngbua");
		places.add("Flekkerhytta");
		places.add("Djupslia");
		places.add("Sandvasshytta");
		places.add("Gammelsetra");
		
		return places;
	}
	
	
	public static boolean samplePlacesContains(PlaceModel place) {
		boolean contains = false;
		List<String> places = buildSamplePlaceNames();
		for(String samplePlaceName : places) {
			if(place.getPlaceType().equalsIgnoreCase("Hut") && samplePlaceName.equalsIgnoreCase(place.getPlaceName())) {
				contains = true;
				break;
			}
		}
		
		return contains;
	}

}

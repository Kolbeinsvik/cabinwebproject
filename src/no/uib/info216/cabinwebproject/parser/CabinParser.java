package no.uib.info216.cabinwebproject.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import no.uib.info216.cabinwebproject.dao.CabinDao;
import no.uib.info216.cabinwebproject.model.CabinModel;
import no.uib.info216.cabinwebproject.model.GeoPointModel;
import no.uib.info216.cabinwebproject.util.SharedData;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class CabinParser {
	
	public static File cabinJsonFile = new File(SharedData.getInstance().getCabinResourcePath());
	
	
	public static List<CabinModel> parseCabins() throws IOException {
//		List<CabinModel> cabins = new ArrayList<CabinModel>();
		CabinDao cabinDao = CabinDao.getInstance();
		
		Scanner sc = new Scanner(CabinParser.cabinJsonFile);
		while(sc.hasNextLine()) {
			String json = sc.nextLine();
			
			if(json.isEmpty()) {
				continue;
			}
			
			CabinModel cabin = CabinParser.parseCabin(json);
			cabinDao.add(cabin);
		}
		sc.close();
		
		return cabinDao.getAll();
	}
	
	public static CabinModel parseCabin(String json) throws IOException {
		JsonFactory jsonFactory = new JsonFactory();
		JsonParser jp = jsonFactory.createParser(json);
		
		if(jp.nextToken() != JsonToken.START_OBJECT) {
			throw new IOException("Invalid JSON");
		}
		
		String name = null;
		float latitude = -1;
		float longitude = -1;
		String url = "";
		float[] geoPoint = {-1, -1};
		
		while(jp.nextToken() != JsonToken.END_OBJECT) {
			jp.nextToken();
			
			switch(jp.getCurrentName()) {
			case "_id":
				jp.skipChildren();
				break;
			case "lisens":
				break;
			case "navn":
				name = jp.getText().trim();
				break;
			case "geojson":
				geoPoint = CabinParser.parseGeo(jp);
				break;
			case "url":
				url = jp.getText();
			default:
				break;
			}
		}
		
		CabinModel cabin = new CabinModel(name, new GeoPointModel(geoPoint[1], geoPoint[0]), url);
		
		return cabin;
	}
	
	
	public static float[] parseGeo(JsonParser jp) throws IOException {
		float[] geoData = new float[2];
		
		while(jp.nextToken() != JsonToken.END_OBJECT) {
			jp.nextToken();
	
			if(jp.isExpectedStartArrayToken()) {
				jp.nextValue();
				geoData[0] = (float) jp.getValueAsDouble();

				jp.nextValue();
				geoData[1] = (float) jp.getValueAsDouble();
				
				jp.nextToken();
			}
		}
		
		return geoData;
	}

}


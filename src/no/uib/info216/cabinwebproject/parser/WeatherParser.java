package no.uib.info216.cabinwebproject.parser;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import no.uib.info216.cabinwebproject.controller.MainController;
import no.uib.info216.cabinwebproject.dao.WeatherDao;
import no.uib.info216.cabinwebproject.dao.AreaDao;
import no.uib.info216.cabinwebproject.model.PlaceModel;
import no.uib.info216.cabinwebproject.model.WeatherModel;
import no.uib.info216.cabinwebproject.util.DateUtil;
import no.uib.info216.cabinwebproject.util.StringUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class WeatherParser {

	private static WeatherDao weatherDao = WeatherDao.getInstance();


	public static void parseWeatherData() {
		List<PlaceModel> places = AreaDao.getPlaces();

		for(PlaceModel place : places) {
			// skips non cabin places
			if(!place.getPlaceType().equalsIgnoreCase("Hut")) {
				continue;
			}

			WeatherModel weather = parseWeatherData(place);

			// to be lifted
			if(weather != null) {
				WeatherParser.weatherDao.add(weather);
			}
		}
	}


	public static WeatherModel parseWeatherData(PlaceModel place) {
		URL forecastUri = StringUtil.encodeUrl(place.getForecastUrl());
		if(forecastUri == null) {
			return null;
		}

		String currentIsoDate = DateUtil.getIsoDate();

		float precipitation = 0.0f;
		float temperature = 0.0f;
		int n = 0;

		try {
			// Creates a document list to iterate from xml
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = null;
			db = dbf.newDocumentBuilder();
			Document doc = null;
			doc = db.parse(new InputSource(forecastUri.openStream()));
			doc.getDocumentElement().normalize();

			// Iterate through the datanodes in yr.no.xml
			NodeList tabNodes = doc.getElementsByTagName("tabular");
			for (int i = 0; i < tabNodes.getLength(); i++) {
				Node tab = tabNodes.item(i);
				NodeList timeNodes = tab.getChildNodes();

				// Iterate through time-nodes (contains date-attributes and data-nodes)
				for (int j = 0; j < timeNodes.getLength(); j++) {
					Node time = timeNodes.item(j);
					try{
						Node from = time.getAttributes().getNamedItem("from");
						Calendar timeDate = DatatypeConverter.parseDate(from.getNodeValue());
						String parsedIsoDate = DateUtil.getIsoDate(timeDate.getTime());

						// We are only interested in data for todays forecast
						if(currentIsoDate.equals(parsedIsoDate)){

							// Get data about precipitation and temperature
							NodeList  timeChild = time.getChildNodes();
							for (int k = 0; k < timeChild.getLength(); k++) {
								Node child = timeChild.item(k);

								if(child.getNodeName().equals("precipitation")) {
									Node value = child.getAttributes().getNamedItem("value");
									float precipitationValue = Float.parseFloat(value.getNodeValue());
									precipitation += precipitationValue;
								}

								if(child.getNodeName().equals("temperature")) {
									Node value = child.getAttributes().getNamedItem("value");
									float temperatureValue = Float.parseFloat(value.getNodeValue());
									temperature += temperatureValue;
									n++;
								}
							}
						}
					}catch(NullPointerException e){
						continue;
					}
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			MainController.debugMsg("Error encountered. Could not update weather for "
					+ place.getPlaceName() + " (" + place.getPlaceType() + ")");
			return null;
		}

		// Add data to the dataset: place, date, total precipitation and average temperature
		//		WeatherModel model = new WeatherModel(place, date, precipitation, (temperature/n));
		//		WeatherXMLParser.dao.add(model);
		//		return model;


		WeatherModel weather = new WeatherModel(place, currentIsoDate, precipitation, (temperature/n));
		return weather;
	}

}
